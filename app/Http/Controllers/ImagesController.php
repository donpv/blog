<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImagesRequest;
use App\Http\Requests\VideoRequest;
use App\Images;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;

class ImagesController extends Controller
{
    public function getlist() {
        $posts = Images::all();
        return view('admin.images.list',compact('posts'));
    }

    public function getAdd() {
        return view('admin.images.add');
    }

    public function postImages(ImagesRequest $request) {
        $images = new Images();
        $images->name = $request->input('name');
        if($request->hasFile('photo')){
            ini_set('memory_limit','256M');
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
            if($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg'){
                $images->type = 'photo';
            } else return redirect()->back()->with('errfile','Chưa hỗ trợ định dạng file vừa upload.')->withInput();

            $file_name = $file->getClientOriginalName();
            $random_file_name = str_random(4).'_'.$file_name;
            while(file_exists('upload/images/'.$random_file_name)){
                $random_file_name = str_random(4).'_'.$file_name;
            }
            $file->move('upload/images',$random_file_name);
            // $file_upload = new File();
            // $file_upload->name = $random_file_name;
            // $file_upload->post_id = $post->id;
            // $file_upload->save();
            $images->photo = 'upload/images/'.$random_file_name;
        }
        $images->link = '';
        $images->save();
        return redirect()->route('list_images');
    }


    public function postVIdeo(VideoRequest $request) {
        $video = new Images();
        $video->type = 'video';
        $video->name = $request->input('name');
        $video->link = $request->input('link');
        $video->photo = '';
        $video->save();
        return redirect()->route('list_images');
    }



    public function delete($id) {
        $post = Images::find($id);
        if( $post ){
            if( $post->user_id == Auth::user()->id || Auth::user()->role == 'admin' ){
                $post->delete();
                Session::flash('flash_success','Xóa thành công.');
                return redirect()->route('list_images');
            } else {
                Session::flash('flash_err','Bạn không có quyền xóa bài.');
                return redirect()->route('list_images');
            }
        } else {
            Session::flash('flash_err','Bài viết không tồn tại.');
        }
        return redirect()->route('list_images');
    }
}
