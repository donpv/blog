<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\File;
use App\Tag;
use App\Admin;

class PagesController extends Controller
{
    public function getindex()
    {
        $cates = Category::where('show','=','1')->get();
        $videos = Post::where('post_type','=','video')->take(5)->orderBy('created_at','des')->get();

        return view('news.pages.home',['cates'=>$cates,'videos'=>$videos]);
    }
    public function getCategory($slug)
    {
      $cate = Category::where('slug', $slug)->first();
      $post = Post::where('category_id',$cate->id)->get();
      $sub_cate = Category::where('parent_id',$cate->id)->get();

      return view('news.pages.category',['cate'=>$cate,'sub_cate'=>$sub_cate,'post'=>$post]);


    }
    public function getPost($slug)
    {
    	$post = Post::where('status',1)->where('slug', $slug)->first();
        if(count($post)==0){
            return view('news.pages.singlepost',['key'=>$slug]);
        } else
        {
            $post->view = $post->view + 1;
            $post_lq = Post::where('status',1)->where('slug','!=', $slug)->where('category_id','=',$post->category_id)->take(5)->get();
            $post->save();
            return view('news.pages.singlepost',compact('post','post_lq'));
        }
    }
    public function getTag($key)
    {
    	$tag = Tag::where('name', $key)->first();
        if(count($tag)==0 || count($tag->posts)==0){
            return view('news.pages.tag',['key'=>$key]);
        } else 
        return view('news.pages.tag',['tag'=>$tag]);
    }
    public function getSearch(Request $request)
    {
        $key= $request->input('keyword');
    	$posts = Post::where('status',1)->where('title', 'like', '%'.$key.'%')->get();
        return view('news.pages.search',['posts'=>$posts,'key'=>$key]);
    }
    public function getAuthor($user)
    {
        $author = Admin::where('name',$user )->first();
        if(count($author)==0 || count($author->posts)==0){
            return view('news.pages.author',['key'=>$user]);
        } else 
        return view('news.pages.author',['author'=>$author]);
    }

    public function getImages() {
        return view('news.pages.photo');
    }

    public function getVideo() {
        return view('news.pages.video');
    }
    public function getContact()
    {
    	return view('news.pages.contact');
    }
}
