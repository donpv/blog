<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use Illuminate\Http\Request;
use App\Setting;
use Session;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::find(1);
        return view('admin.setting.list', compact('setting'));
    }

    public function postSetting(SettingRequest $request)
    {
        $setting = new Setting();
        $setting->name = $request->input('name');

        $setting->slogan = $request->input('slogan');
        $setting->email = $request->input('email');
        $setting->phone = $request->input('phone');
        $setting->address = $request->input('address');
        //Upload Image
        if ($request->hasFile('banner')) {
            $file = $request->file('banner');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
            if ($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg') {
                $file_name = $file->getClientOriginalName();
                $random_file_name = str_random(4) . '_' . $file_name;
                while (file_exists('upload/setting/' . $random_file_name)) {
                    $random_file_name = str_random(4) . '_' . $file_name;
                }
                $file->move('upload/setting', $random_file_name);

                $setting->banner = 'upload/setting/' . $random_file_name;
            } else return redirect()->back()->with('errfile', 'Chưa hỗ trợ định dạng file vừa upload.')->withInput();


        } else $setting->banner = '';
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file

            if ($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg') {
                $file_name = $file->getClientOriginalName();
                $random_file_name = str_random(4) . '_' . $file_name;
                while (file_exists('upload/setting/' . $random_file_name)) {
                    $random_file_name = str_random(4) . '_' . $file_name;
                }
                $file->move('upload/setting', $random_file_name);
                $setting->logo = 'upload/setting/' . $random_file_name;
            } else return redirect()->back()->with('errfile', 'Chưa hỗ trợ định dạng file vừa upload.')->withInput();


        } else $setting->logo = '';

        $setting->save();
        Session::flash('flash_success','Thay đổi thành công.');
        return redirect()->back();

    }

    public function postUpdate(Request $request) {
        $setting = Setting::find(1);
        $setting->name = $request->input('name');

        $setting->slogan = $request->input('slogan');
        $setting->email = $request->input('email');
        $setting->phone = $request->input('phone');
        $setting->address = $request->input('address');
        //Upload Image
        if ($request->hasFile('banner')) {
            $file = $request->file('banner');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
            if ($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'gif') {
                $file_name = $file->getClientOriginalName();
                $random_file_name = str_random(4) . '_' . $file_name;
                while (file_exists('upload/setting/' . $random_file_name)) {
                    $random_file_name = str_random(4) . '_' . $file_name;
                }
                $file->move('upload/setting', $random_file_name);

                $setting->banner = 'upload/setting/' . $random_file_name;
            } else return redirect()->back()->with('errfile', 'Chưa hỗ trợ định dạng file vừa upload.')->withInput();


        }
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file

            if ($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg') {
                $file_name = $file->getClientOriginalName();
                $random_file_name = str_random(4) . '_' . $file_name;
                while (file_exists('upload/setting/' . $random_file_name)) {
                    $random_file_name = str_random(4) . '_' . $file_name;
                }
                $file->move('upload/setting', $random_file_name);
                $setting->logo = 'upload/setting/' . $random_file_name;
            } else return redirect()->back()->with('errfile', 'Chưa hỗ trợ định dạng file vừa upload.')->withInput();


        }

        $setting->save();
        Session::flash('flash_success','Thay đổi thành công.');
        return redirect()->back();

    }
}
