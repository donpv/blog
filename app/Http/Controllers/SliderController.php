<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SliderController extends Controller
{
    public function getlist() {
       $slider = Slider::all();
       return view('admin.slider.list',compact('slider'));

    }

    public function getAdd() {
        return view('admin.slider.add');
    }

    public function postAdd(Request $request) {
        $slider = new Slider();
        $slider->name = $request->input('name');
        $slider->discription = $request->input('discription');
        $slider->link = $request->input('link');
        $slider->status = 0;
        //Upload Image
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
            if($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg'){
                $file_name = $file->getClientOriginalName();
                $random_file_name = str_random(4).'_'.$file_name;
                while(file_exists('upload/sliders/'.$random_file_name)){
                    $random_file_name = str_random(4).'_'.$file_name;
                }
                $file->move('upload/sliders',$random_file_name);
                // $file_upload = new File();
                // $file_upload->name = $random_file_name;
                // $file_upload->link = 'upload/posts/'.$random_file_name;
                // $file_upload->post_id = $post->id;
                // $file_upload->save();
                $slider->photo = 'upload/sliders/'.$random_file_name;
            } else return redirect()->back()->with('errfile','Chưa hỗ trợ định dạng file vừa upload.')->withInput();


        } else $slider->photo='';
        $slider->save();
        Session::flash('flash_success','Thêm tin tức thành công.');
        return redirect()->route('list-slider');
    }




    public function getUpdate($id) {
        $slider = Slider::find($id);
        return view('admin.slider.edit',compact('slider'));
    }

    public function postUpdate(Request $request,$id) {
        $slider = Slider::find($id);
        $slider->name = $request->input('name');
        $slider->link = $request->input('link');
        $slider->discription = $request->input('discription');
        $slider->status = $request->input('status');
        //Upload Image
        if($request->hasFile('photo')){
            ini_set('memory_limit','256M');
            $file = $request->file('photo');
            $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
            if($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg'){
                $file_name = $file->getClientOriginalName();
                $random_file_name = str_random(4).'_'.$file_name;
                while(file_exists('upload/posts/'.$random_file_name)){
                    $random_file_name = str_random(4).'_'.$file_name;
                }
                $file->move('upload/sliders',$random_file_name);
                // $file_upload = new File();
                // $file_upload->name = $random_file_name;
                // $file_upload->post_id = $post->id;
                // $file_upload->save();
                $slider->photo = 'upload/sliders/'.$random_file_name;
            } else return redirect()->back()->with('errfile','Chưa hỗ trợ định dạng file vừa upload.')->withInput();


        }

        $slider->save();


        Session::flash('flash_success','Thay đổi thành công.');
        return redirect()->route('list-slider');
    }





    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            $slider = Slider::find($request->input('id'));
            if( $slider ){
                if( Auth::user()->role == 'admin' ){
                    if($request->input('status')== 0 || $request->input('status')==1 ){
                        $slider->status =$request->input('status');
                        $slider->save();
                        return 'ok';
                    } else { return 'Sai trạng thái.';}
                } else { return 'Bạn không đủ quyền'; }
            } else { return 'Bài viết không tồn tại.'; }
        }
    }

    public function delete($id)
    {
        $slider = Slider::find($id);
        if( $slider ) {

            $slider->delete();
            Session::flash('flash_success', 'Xóa thành công.');
            return redirect()->route('list-slider');

        }
        return redirect()->route('list-post');
    }
}
