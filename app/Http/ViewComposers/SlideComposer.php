<?php

namespace App\Http\ViewComposers;

use App\Slider;
use Illuminate\View\View;


class SlideComposer
{
    /**
     * The user repository implementation.
     *
     * @var  UserRepository
     */

    /**
     * Create a new profile composer.
     *
     * @param    UserRepository  $users
     * @return  void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        //$this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param    View  $view
     * @return  void
     */
    public function compose(View $view)
    {
        $slider = Slider::where('status',0)->orderBy('id','des')->limit(10)->get();
        $view->with('slider',$slider);
    }
}