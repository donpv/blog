<?php
/**
 * Created by PhpStorm.
 * User: phamdon
 * Date: 6/10/18
 * Time: 20:27
 */

namespace App\Http\ViewComposers;


use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SettingComposer
{
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        //$this->users = $users;
    }

    public function compose(View $view)
    {
        $setting = DB::table('settings')->where('id','=',1)->get();
        $view->with('setting',$setting);
    }
}