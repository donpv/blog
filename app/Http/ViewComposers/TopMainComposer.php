<?php
/**
 * Created by PhpStorm.
 * User: Mr.Shin
 * Date: 2018-06-07
 * Time: 오후 2:00
 */

namespace App\Http\ViewComposers;
use Illuminate\View\View;
use DB;

class TopMainComposer
{
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        //$this->users = $users;
    }

    public function compose(View $view)
    {
        $hot = DB::table('posts')->where('hot','=',1)->where('status','=',1)->get();
        $view->with('hot',$hot);
    }

}