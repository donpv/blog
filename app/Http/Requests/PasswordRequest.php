<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ' required|confirmed|min:8',
            'password_confirmation' =>'required|min:8'
        ];
    }

    public function messages()
    {
        return [
            'password.required' =>'Mật khẩu không được để trống',
            'password.confirmed' =>'Mật khẩu nhập lại không khớp',
            'password.min' => 'Mật khẩu tối thiểu 8 ký tự',
            'password_confirmation.required' =>'Bạn phải nhập lại mật khẩu',
            'password_confirmation.min' => 'Mật khẩu nhập lại tối thiểu là 8 ký tự '
        ];

    }
}
