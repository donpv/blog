<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>'required|email',
            'phone' => 'required|min:10',
            'address' => 'required',
            'name' =>'required',

        ];
    }

    public function messages()
    {
        return [
            'email.required' =>'Email không được để trống',
            'email.email' =>'Nhập đúng định dạng email',
            'phone.required' =>'Số điện thoại không được để trống',
            'phone.min' =>'Số điện thoại tối thiểu 10 số',
            'address.required' =>'Địa chỉ không được để trống',
            'name.required' =>'Tên công ty không được để trống'
        ];
    }
}
