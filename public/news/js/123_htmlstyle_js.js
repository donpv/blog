$("[name='select_style']").live("click", function(){
    var i = $("[name='select_style']").index($(this));
	var current_obj = $(this);
    $("[name='select_style']").each(function(index){
        if(i!=index){
            $(this).find(".root").hide();
        }
    });
    current_obj.find(".root").slideToggle(150);
});
$("[name='select_style']").find(".root").find("li").live("click", function(){
    var parent_obj = $(this).parent().parent();
    var value = $(this).attr("idata");
	var value_show = $(this).find("span").html();
    parent_obj.find(".current_value").attr("value",value);
    parent_obj.find(".current_value").html(value_show);
    parent_obj.find("input:last").val(value);
    parent_obj.find("li").removeAttr("class");
    $(this).attr("class","selected");
	var call_func = parent_obj.attr("funct_name");
	if(call_func!=""&&call_func!=undefined){
		//var param_function = parent_obj.attr("funct_param");
		var fn = window[call_func];
		fn($(this));
	}
});

$("[name='radio_style']").find(".root").find("li").live("click", function(){
    var current_obj = $(this);
	current_obj.parent().find("li").removeAttr("class");
	current_obj.attr("class","active");
	var select_value = current_obj.attr("idata");
	current_obj.parent().parent().find("input:last").val(select_value);
});

$(document).bind('click', function(e) {  
    var $clicked = $(e.target);
	if($clicked.parents().attr("name")!="select_style"){
		$("[name='select_style']").find(".root").hide();
	}
    /*if (!$clicked.parents().hasClass("select_style2")){
		$("[name='select_style']").find(".root").hide();
    }*/
});
$(".checkbox_style1").live("click", function(){
    var current_value = $(this).find("input").val();
    if(current_value==0){// Current off
        $(this).find(".radio").find("span").attr("class","on");
        $(this).find("input").val("1");
    }
    else{
        $(this).find(".radio").find("span").attr("class","off");
        $(this).find("input").val("0");
    }
});