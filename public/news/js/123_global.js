isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
function format_ckeditor(id,minicontrol,width,height){
    try{
        config = {startupFocus:false,toolbar:[['Format', 'Styles','Font','FontSize','Bold','Italic','Underline','Strike','NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','TextColor','BGColor'],['RemoveFormat','Source'],['Link','Unlink','Image','Table','Iframe','Flash','Find']],uiColor : '#F4F5F7',width: width,height: height};
        if(minicontrol==true){
            config = {startupFocus:false,toolbar:[['Bold','Italic','Underline','Undo','Redo','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink','TextColor','BGColor'],['Font','FontSize','RemoveFormat','Source']],uiColor : '#F4F5F7', width: width,height: height};
        }
        CKEDITOR.replace(id, config);
    }catch(err){}
}
function format_object_to_ckeditor(obj,minicontrol,width,height){
    try{
        config = {startupFocus:false,toolbar:[['Format', 'Styles','Font','FontSize','Bold','Italic','Underline','Strike','NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','TextColor','BGColor'],['RemoveFormat','Source'],['Link','Unlink','Image','Table','Iframe','Flash','Find']],uiColor : '#F4F5F7',width: width,height: height};
        if(minicontrol==true){
            config = {startupFocus:false,toolbar:[['Bold','Italic','Underline','Undo','Redo','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink','TextColor','BGColor'],['Font','FontSize','RemoveFormat','Source']],uiColor : '#F4F5F7', width: width,height: height};
        }
        obj.ckeditor(config);
    }catch(err){}
}
$('.input_style').find("select").live('keyup', function (e) {
    var key = e.keyCode || e.which;
    if(key==13){
        var inputs = $(this).closest('form').find('.input_style').find(":focusable");
        inputs.eq( inputs.index(this)+ 1 ).focus();
        return;
    }
    if($(this).attr("require")=="true"){
        if($(this).val()==$(this).attr("default")){
            var require_text = $(this).attr("require_text");
            if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
            $(this).parent().find(".warning").show();
        }
        else{
            $(this).parent().find(".warning").hide();
        }
    }
    if($(this).attr("email_require")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateEmail($(this).val())){
                var email_require_text = $(this).attr("email_require_text");
                $(this).parent().find(".warning").find("span:first").html(email_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateNumber($(this).val())){
                var numberic_require_text = $(this).attr("require_numberic_text");
                $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                if($(this).attr("formar_number")=="false"){
                    
                }
                else{
                    $(this).val(addCommas($(this).val()));
                }
                $(this).parent().find(".warning").hide();
            }
        }
    }
});
$('.input_style').find("input").live('keyup', function (e) {
    var key = e.keyCode || e.which;
    if(key==13){
        var inputs = $(this).closest('form').find('.input_style').find(":focusable");
        inputs.eq( inputs.index(this)+ 1 ).focus();
        return;
    }
    if($(this).attr("require")=="true"){
        if($(this).val()==$(this).attr("default")){
            var require_text = $(this).attr("require_text");
            if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
            $(this).parent().find(".warning").show();
        }
        else{
            $(this).parent().find(".warning").hide();
        }
    }
    if($(this).attr("email_require")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateEmail($(this).val())){
                var email_require_text = $(this).attr("email_require_text");
                $(this).parent().find(".warning").find("span:first").html(email_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateNumber($(this).val())){
                var numberic_require_text = $(this).attr("require_numberic_text");
                $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                if($(this).attr("formar_number")=="false"){
                    
                }
                else{
                    $(this).val(addCommas($(this).val()));
                }
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("alow_numeric")=="false"){
        if($(this).val()!=$(this).attr("default")){
            var regex_numeric_check = /\d/g;
            if(regex_numeric_check.test($(this).val())){
                var alow_numeric_text = $(this).attr("alow_numeric_text");
                $(this).parent().find(".warning").find("span:first").html(alow_numeric_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
});
$('.input_style').find("textarea").live('keyup', function (e) {
    if($(this).attr("require")=="true"){
        if($(this).val()==$(this).attr("default")){
            var require_text = $(this).attr("require_text");
            if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
            $(this).parent().find(".warning").show();
        }
        else{
            $(this).parent().find(".warning").hide();
        }
    }
    if($(this).attr("email_require")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateEmail($(this).val())){
                var email_require_text = $(this).attr("email_require_text");
                $(this).parent().find(".warning").find("span:first").html(email_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateNumber($(this).val())){
                var numberic_require_text = $(this).attr("require_numberic_text");
                $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                if($(this).attr("formar_number")=="false"){
                    
                }
                else{
                    $(this).val(addCommas($(this).val()));
                }
                $(this).parent().find(".warning").hide();
            }
        }
    }
});
$('.input_style').find("select").live('focus', function (e) {
    var focus_class = "select_focus";
    if($(this).attr('class') !== undefined){
        var str_class = $(this).attr("class");
        if(str_class!=''){
            focus_class = str_class + " " + focus_class;
        }
    }
    $(this).attr("class",focus_class);
    
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()==0){
            $(this).val('');
        }
    }
});
$('.input_style').find("select").live('blur', function (e) {
    if($(this).attr('class') !== undefined){
        var str_class = $(this).attr("class");
        str_class = str_class.replace(" select_focus","");
        str_class = str_class.replace("select_focus","");
        $(this).attr("class",str_class);
    }
    if($(this).attr("require")=="true"){
        if($(this).val()==$(this).attr("default")){
            var require_text = $(this).attr("require_text");
            if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
            $(this).parent().find(".warning").show();
        }
        else{
            $(this).parent().find(".warning").hide();
        }
    }
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateNumber($(this).val())){
                var numberic_require_text = $(this).attr("require_numberic_text");
                $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("email_require")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateEmail($(this).val())){
                var email_require_text = $(this).attr("email_require_text");
                $(this).parent().find(".warning").find("span:first").html(email_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    /*
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()==''){
            $(this).val('0');
        }
    }
    */
    //if($(this).parent().find("ul.suggestion").length>0){
        //$(this).parent().find("ul.suggestion").hide();
    //}
});
$('.input_style').find("input").live('focus', function (e) {
    var focus_class = "input_focus";
    if($(this).attr('class') !== undefined){
        var str_class = $(this).attr("class");
        if(str_class!=''){
            focus_class = str_class + " " + focus_class;
        }
    }
    $(this).attr("class",focus_class);
    
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()==0){
            $(this).val('');
        }
    }
});
$('.input_style').find("input").live('blur', function (e) {
    if($(this).attr('class') !== undefined){
        var str_class = $(this).attr("class");
        str_class = str_class.replace(" input_focus","");
        str_class = str_class.replace("input_focus","");
        $(this).attr("class",str_class);
    }
    if($(this).attr("require")=="true"){
        if($(this).val()==$(this).attr("default")){
            var require_text = $(this).attr("require_text");
            if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
            $(this).parent().find(".warning").show();
        }
        else{
            $(this).parent().find(".warning").hide();
        }
    }
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateNumber($(this).val())){
                var numberic_require_text = $(this).attr("require_numberic_text");
                $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("email_require")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateEmail($(this).val())){
                var email_require_text = $(this).attr("email_require_text");
                $(this).parent().find(".warning").find("span:first").html(email_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    /*
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()==''){
            $(this).val('0');
        }
    }
    */
    if($(this).attr("alow_numeric")=="false"){
        if($(this).val()!=$(this).attr("default")){
            var regex_numeric_check = /\d/g;
            if(regex_numeric_check.test($(this).val())){
                var alow_numeric_text = $(this).attr("alow_numeric_text");
                $(this).parent().find(".warning").find("span:first").html(alow_numeric_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    //if($(this).parent().find("ul.suggestion").length>0){
        //$(this).parent().find("ul.suggestion").hide();
    //}
});
$('.input_style').find("textarea").live('blur', function (e) {
    if($(this).attr("require")=="true"){
        if($(this).val()==$(this).attr("default")){
            var require_text = $(this).attr("require_text");
            if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
            $(this).parent().find(".warning").show();
        }
        else{
            $(this).parent().find(".warning").hide();
        }
    }
    if($(this).attr("email_require")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateEmail($(this).val())){
                var email_require_text = $(this).attr("email_require_text");
                $(this).parent().find(".warning").find("span:first").html(email_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
    if($(this).attr("require_numberic")=="true"){
        if($(this).val()!=$(this).attr("default")){
            if(!validateNumber($(this).val())){
                var numberic_require_text = $(this).attr("require_numberic_text");
                $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                $(this).parent().find(".warning").show();
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    }
});
function global_form_before_process(frm){
    strreturn = true;
    var obj_focus = 1;
    frm.find(".warning").hide();
    frm.find(".input_style").find("input").each(function(index){
        
        if($(this).attr("require")=="true"){
            if($(this).val()==$(this).attr("default")){
                var require_text = $(this).attr("require_text");
                if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
                $(this).parent().find(".warning").show();
                strreturn = false;
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
        if($(this).attr("require_numberic")=="true"){
            if($(this).val()!=$(this).attr("default")){
                if(!validateNumber($(this).val())){
                    var numberic_require_text = $(this).attr("require_numberic_text");
                    $(this).parent().find(".warning").find("span:first").html(numberic_require_text);
                    $(this).parent().find(".warning").show();
                }
                else{
                    $(this).parent().find(".warning").hide();
                }
            }
        }
        if($(this).attr("email_require")=="true"){
            if($(this).val()!=$(this).attr("default")){
                if(!validateEmail($(this).val())){
                    var email_require_text = $(this).attr("email_require_text");
                    $(this).parent().find(".warning").find("span:first").html(email_require_text);
                    $(this).parent().find(".warning").show();
                    strreturn = false;
                }
                else{
                    $(this).parent().find(".warning").hide();
                }
            }
        }
        if($(this).attr("exists_prevent")=="true"){
                    if($(this).attr("exists_error")=="true"){
                        $(this).parent().find(".warningExists").show();
                        strreturn = false;
                    }
                }
    });
    frm.find(".input_style").find("select").each(function(index){
        if($(this).attr("require")=="true"){
            if($(this).val()==$(this).attr("default")){
                var require_text = $(this).attr("require_text");
                if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
                $(this).parent().find(".warning").show();
                strreturn = false;
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
    });
    frm.find(".input_style").find("textarea").each(function(index){
        $(this).val($.trim($(this).val()));
        if($(this).attr("require")=="true"){
            if($(this).val()==$(this).attr("default")){
                var require_text = $(this).attr("require_text");
                if(require_text!=""){$(this).parent().find(".warning").find("span:first").html(require_text);}
                $(this).parent().find(".warning").show();
                strreturn = false;
            }
            else{
                $(this).parent().find(".warning").hide();
            }
        }
        if($(this).attr("email_require")=="true"){
            if($(this).val()!=$(this).attr("default")){
                if(!validateEmail($(this).val())){
                    var email_require_text = $(this).attr("email_require_text");
                    $(this).parent().find(".warning").find("span:first").html(email_require_text);
                    $(this).parent().find(".warning").show();
                    strreturn = false;
                }
                else{
                    $(this).parent().find(".warning").hide();
                }
            }
        }
        if($(this).attr("exists_prevent")=="true"){
                    if($(this).attr("exists_error")=="true"){
                        $(this).parent().find(".warningExists").show();
                        strreturn = false;
                    }
                }
    });
    
    if(strreturn==false){
        if(frm.find(".captcha_image").length>0){
            frm.find(".captcha_image").attr("src",base_folder+"captcha/captcha.php?"+Math.random());
            frm.find("[name='security_code']").val("");
        }
        if(obj_focus!=1){obj_focus.focus();}
    }
    else{
        show_alert_doing2();
    }
    return strreturn;
}
function extract_tagList_from_div(idElememt){
    var strTags = '';
    $("#"+idElememt).find("a").each(function(){
        var strValue = $.trim($(this).html());
        strTags += ';' + strValue;
    });
    if(strTags.indexOf(";")==0){strTags=strTags.substring(1);}
    return strTags;
}
function global_form_after_process(formName,status,message,url,obj_error){
    var frm = $("[name='"+formName+"']");
    close_alert_doing2();
    if(frm.find(".captcha_image").length>0){frm.find(".captcha_image").attr("src",base_folder+"captcha/captcha.php?"+Math.random());}
    if(frm.find("[name='security_code']").length>0){frm.find("[name='security_code']").val("");}
    if(status==1){
        alert(message);
        if(url==""){url = base_folder;}
        window.location = url;
    }
    if(status==3){
        alert(message);
        window.location = path_info;
    }
    if(status==2){
        array = obj_error.split(",");
        message = message.split(";");
        for(var i=0;i<array.length;i++){
            frm.find("[name='"+array[i]+"']").parent().find(".warning").find("span:first").html(message[i]);
            frm.find("[name='"+array[i]+"']").parent().find(".warning").show();
            if(i==0){
                frm.find("[name='"+array[i]+"']").focus();
            }
        }
    }
    if(status==5){
        alert(message);
        var doitCheck = 0;
        if($("#doitInput").is(':checked')){
            doitCheck = 1;
        }
        $("[name='"+formName+"']")[0].reset();
        if(doitCheck==1){
            $("#doitInput").attr("checked","checked");
            $("#source_of_image").attr("disabled","disabled");
        }
        else{
            $("#doitInput").removeAttr("checked");
            $("#source_of_image").removeAttr("disabled");
        }
    }
    if(status==10){ // Reset to default
        frm.find("textarea").each(function(indextextarea){
            if($(this).attr("reset")=="true"){
                $(this).val($(this).attr("default"));
            }
        });
        frm.find("input").each(function(indexinput){
            if($(this).attr("reset")=="true"){
                $(this).val($(this).attr("default"));
            }
        });
                try{
                    for ( instance in CKEDITOR.instances ){
                        CKEDITOR.instances[instance].updateElement();
                        CKEDITOR.instances[instance].setData('');
                    }
                }
                catch(err){
                    
                }
                close_alert_doing2();
                show_message_box('Thông báo',message,'success',2000,"close_window_box");
                
            }
    if(status==15){ // Redirect to NEW CONTENT
        redirect_url = url;
        show_message_box('Thông báo',message,'process',1000,"redirect_to_url_by_ajax");
    }
    if(status==16){ // Close windowbox and reload content
        close_alert_doing2();
        show_message_box('Thông báo',message,'process',2000,"close_window_box");
    }
    if(status==17){ // SHOW ERROR AND Close windowbox
        close_alert_doing2();
        show_message_box('Có lỗi xảy ra',message,'error',5000,"close_window_box");
    }
    if(status==0){
        alert(message);
    }
}
function addCommas(num){
    num = num.toString().replace(/\$|\./g,'');
    if(isNaN(num))
    num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    num = Math.floor(num/100).toString();
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+'.'+ num.substring(num.length-(4*i+3));
    return (((sign)?'':'-') + num);
}
function count_word_in_string(s){
    if(s==""){return 0;}
    s = s.replace(/(^\s*)|(\s*$)/gi,"");
    s = s.replace(/[ ]{2,}/gi," ");
    s = s.replace(/\n /,"\n");
    return s.split(' ').length;
}
function count_number_character_of_obj(obj){
    var length = obj.val().length;
    var idTarget = obj.attr("target_length");
    $("#"+idTarget).html('('+length+')');
}
function count_word_character_of_obj(obj){
    var s = obj.val();
    var count = 0;
    if(s==""){count = 0;}
    else{
        s = s.replace(/(^\s*)|(\s*$)/gi,"");
        s = s.replace(/[ ]{2,}/gi," ");
        s = s.replace(/\n /,"\n");
        count = s.split(',').length;
    }
    var idTarget = obj.attr("target_length");
    $("#"+idTarget).html('('+count+')');
}

function filterTextFromString(strInut){
    var number = strInut.replace(/\D/g,'');
    return number;
}
function get_content_from_tags(tag_start,tag_end,htmlcontent){
    var strReturn = '';
    if(htmlcontent.indexOf(tag_start)>=0 && htmlcontent.lastIndexOf(tag_end)>htmlcontent.indexOf(tag_start)){
        strReturn = htmlcontent.substring(htmlcontent.indexOf(tag_start)+tag_start.length);
        strReturn = strReturn.substring(0,strReturn.indexOf(tag_end));
        strReturn = $.trim(strReturn);
    }
    return strReturn;
}
function validateUrl(url) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return regexp.test(url);
}
function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
        return false;
    }
    else{
        return true;
    }
}
function validateNumber(number) {
    number = number.toString().replace(/\$|\./g,'');
    if(isNaN(number)){
        return false;
    }
    else{
        return true;
    }
}
/**
*
*  MD5 (Message-Digest Algorithm)
*
**/    
var MD5 = function (string) {
 
    function RotateLeft(lValue, iShiftBits) {
        return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
    }
 
    function AddUnsigned(lX,lY) {
        var lX4,lY4,lX8,lY8,lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
     }
 
     function F(x,y,z) { return (x & y) | ((~x) & z); }
     function G(x,y,z) { return (x & z) | (y & (~z)); }
     function H(x,y,z) { return (x ^ y ^ z); }
    function I(x,y,z) { return (y ^ (x | (~z))); }
 
    function FF(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function GG(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function HH(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function II(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1=lMessageLength + 8;
        var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
        var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
        var lWordArray=Array(lNumberOfWords-1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while ( lByteCount < lMessageLength ) {
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount-(lByteCount % 4))/4;
        lBytePosition = (lByteCount % 4)*8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
        lWordArray[lNumberOfWords-2] = lMessageLength<<3;
        lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
        return lWordArray;
    };
 
    function WordToHex(lValue) {
        var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
        for (lCount = 0;lCount<=3;lCount++) {
            lByte = (lValue>>>(lCount*8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
        }
        return WordToHexValue;
    };
 
    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
 
        for (var n = 0; n < string.length; n++) {
 
            var c = string.charCodeAt(n);
 
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
 
        }
 
        return utftext;
    };
 
    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;
 
    string = Utf8Encode(string);
 
    x = ConvertToWordArray(string);
 
    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;
 
    for (k=0;k<x.length;k+=16) {
        AA=a; BB=b; CC=c; DD=d;
        a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
        d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
        c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
        b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
        a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
        d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
        c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
        b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
        a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
        d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
        c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
        b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
        a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
        d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
        c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
        b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
        a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
        d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
        c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
        b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
        a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
        d=GG(d,a,b,c,x[k+10],S22,0x2441453);
        c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
        b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
        a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
        d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
        c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
        b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
        a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
        d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
        c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
        b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
        a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
        d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
        c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
        b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
        a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
        d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
        c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
        b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
        a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
        d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
        c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
        b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
        a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
        d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
        c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
        b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
        a=II(a,b,c,d,x[k+0], S41,0xF4292244);
        d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
        c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
        b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
        a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
        d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
        c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
        b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
        a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
        d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
        c=II(c,d,a,b,x[k+6], S43,0xA3014314);
        b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
        a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
        d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
        c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
        b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
        a=AddUnsigned(a,AA);
        b=AddUnsigned(b,BB);
        c=AddUnsigned(c,CC);
        d=AddUnsigned(d,DD);
    }
 
    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);
 
    return temp.toLowerCase();
}