function slideFade(obj,auto,duration,start,fadeIn,fadeOut){
	this.start = start;this.currentPosition=0;this.slideBarWid=675;this.numInShow=5;this.currentIndexShow=start;
	this.lastIndex=start;
	this.busy = false;
	this.initPro;
    this.interval = duration;
    this.obj = obj;
	this.auto = auto;
	this.fadeIn = fadeIn;
	this.fadeOut = fadeOut;
	this.slide_number = obj.find("ul.slider").find("li").length;
    this.startSlide = function(){
        initInterval(this);
    };
	if(this.auto==true){
		this.startSlide();
		this.obj.find("ul.button_bar").find("li.start").hide();
		this.obj.find("ul.button_bar").find("li.stop").show();
	}
	else{
		this.obj.find("ul.button_bar").find("li.start").show();
		this.obj.find("ul.button_bar").find("li.stop").hide();
	}
	function createInitPro(instance){
		instance.initPro = setInterval(function(){
			if(instance.start==instance.slide_number-1){
				instance.start = 0;
			}
			else{
				instance.start = instance.start + 1;
			}
			set_slide(instance,instance.start,instance.fadeIn,instance.fadeOut);
		},instance.interval);
	}
    function initInterval(instance){
		set_slide(instance,instance.start,instance.fadeIn,instance.fadeOut);
		createInitPro(instance);
	}
	function set_slide(instance,index,fadeIn,fadeOut){
        instance.obj.find("ul.slider").find("li").fadeOut(fadeOut);
        instance.obj.find("ul.slider").find("li:eq("+index.toString()+")").fadeIn(fadeIn);
        instance.obj.find("ul.navigator").find("li").removeAttr("class");
        instance.obj.find("ul.navigator").find("li:eq("+index.toString()+")").attr("class","active");
		if(index==instance.lastIndex){
			
		}
		else{
			var item = index+1;
			var NumShow = Math.ceil(item/instance.numInShow);
			NumShow -= 1;
			var pos = instance.slideBarWid*NumShow;
			set_position_of_show(instance,pos,fadeIn,fadeOut);
		}
		instance.lastIndex = index;
    }
	function set_position_of_show(instance,pos,fadeIn,fadeOut){
		instance.obj.find("ul.navigator").stop().animate({
                left: "-"+pos+"px"
            },1000,"linear",function(){
				instance.busy=false;
            });
	}
	var instance1 = this;
	this.obj.find("ul.navigator").find("li").click(function(){
		var index = instance1.obj.find("ul.navigator").find("li").index($(this));
		instance1.start = index;
		//instance1.obj.find("ul.navigator").find("li").removeAttr("class");
		set_slide(instance1,index,instance1.fadeIn,instance1.fadeOut);
	});
	this.obj.find(".button_bar").find("li.stop").click(function(){
		instance1.obj.find("ul.button_bar").find("li.start").show();
		instance1.obj.find("ul.button_bar").find("li.stop").hide();
		clearInterval(instance1.initPro);
	});
	this.obj.find(".button_bar").find("li.start").click(function(){
		instance1.obj.find("ul.button_bar").find("li.start").hide();
		instance1.obj.find("ul.button_bar").find("li.stop").show();
		//set_slide(instance1,instance1.start,instance1.fadeIn,instance1.fadeOut);
		createInitPro(instance1);
	});
	this.obj.find(".button_bar").find("li.prev").click(function(){
		//if(instance1.busy==false){
			//instance1.busy=true;
			clearInterval(instance1.initPro);
			if(instance1.start==0){
				instance1.start = instance1.slide_number-1;
			}
			else{
				instance1.start = instance1.start - 1;
			}
			set_slide(instance1,instance1.start,instance1.fadeIn,instance1.fadeOut);
			createInitPro(instance1);
		//}
	});
	this.obj.find(".button_bar").find("li.next").click(function(){
		clearInterval(instance1.initPro);
		if(instance1.start==instance1.slide_number-1){
			instance1.start = 0;
		}
		else{
			instance1.start = instance1.start + 1;
		}
		set_slide(instance1,instance1.start,instance1.fadeIn,instance1.fadeOut);
		createInitPro(instance1);
	});
}
function slideLR(obj,autopPlay,duration,start,timeeffect,typeeffect,itemSpace){	
	var current_slide = start;
	this.autopPlay = autopPlay;
	this.start = start;
	this.timeeffect = timeeffect;
	this.typeeffect = typeeffect;
	this.itemSpace  = itemSpace;
	if(isNaN(this.itemSpace)){this.itemSpace=0;}
	this.busy = false;
	this.initPro;
    this.interval = duration;
    this.obj = obj;
	this.width = obj.find(".preview_container").width();
	this.height = obj.find(".preview_container").height();	
	this.leftPos = this.width + this.itemSpace;
	this.slide_number = obj.find("ul.item").find("li.itemLi").length;
	this.obj.find(".index_bar_current").html(start+1);
	
	var height_box = obj.parents().find(".hover_img_box").height();
	//alert(height_box);
	var height_preview = height_box - 218;
	this.obj.find("ul.preview").css("height",+height_preview+"px");
	
	
	this.Initcomponent = function(){
		if(this.slide_number>0){
			//this.obj.find(".preview").find("ul").append("<li>" + this.obj.find(".container").find("li:eq(0)").html() + "</li>");
		}
	};
	this.startSlide = function(){
        initLR(this);
    };	
	function initLR(instance){
		instance.initPro = setInterval(function(){
			if(instance.busy==false){
				instance.busy=true;
				if(instance.start==instance.slide_number-1){
					instance.start = 0;
				}
				else{
					instance.start = instance.start + 1;
				}
				set_slide("next",instance,instance.start);
				/*
				if(instance.start==0){
					instance.start = instance.slide_number - 1;
				}
				else{
					instance.start = instance.start - 1;
				}
				set_slide("pre",instance,instance.start);
				*/
			}
		},instance.interval);
	}
	function set_index_bar_value(instance){
		var index_number = instance1.start+1;
		instance.obj.find(".index_bar_current").html(index_number);
	}
	function set_slide(type,instance,index){
		instance.obj.find("ul.navigator").find("li").removeAttr("class");
		instance.obj.find("ul.navigator").find("li:eq("+index.toString()+")").attr("class","active");
		var add_value = '<li class="itemLi">'+instance.obj.find("ul.item").find("li.itemLi:eq("+index.toString()+")").html()+'</li>';
		if(type=="next"){
			instance.obj.find("ul.preview").append(add_value);
			instance.obj.find("ul.preview").stop().animate({
                left: "-"+instance.leftPos.toString()+"px"
            },instance.timeeffect,instance.typeeffect,function(){
                instance.obj.find("ul.preview").find("li.itemLi:eq(0)").remove();
                instance.obj.find("ul.preview").css("left","0px");
				instance.busy=false;
				//initLR(instance);
            });
		}
		else{
			instance.obj.find("ul.preview").prepend(add_value);
            instance.obj.find("ul.preview").css("left","-"+instance.leftPos.toString()+"px");
            instance.obj.find("ul.preview").stop().animate({
                left: "0px"
            },instance.timeeffect,instance.typeeffect,function(){
                instance.obj.find("ul.preview").find("li.itemLi:eq(1)").remove();
				instance.busy=false;
				//initLR(instance);
            });
		}
		
		if(instance.obj.find("ul.currentNum").find("li.current").length>0){
			instance.obj.find("ul.currentNum").find("li.current").html(index+1);
		}
		set_index_bar_value(instance);
	}
	if(this.autopPlay==true){this.startSlide();}
	var instance1 = this;
	
	this.obj.find("ul.navigator").find("li").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			var index = instance1.obj.find("ul.navigator").find("li").index($(this));
			if(index>instance1.start){
				instance1.start = index;
				set_slide("next",instance1,instance1.start);
			}
			if(index<instance1.start){
				instance1.start = index;
				set_slide("pre",instance1,instance1.start);
			}
			initLR(instance1);
		}
	});
	
	this.obj.find(".button_bar").find("li.left").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			instance1.obj.find("ul.button_bar").find("li.start").show();
			instance1.obj.find("ul.button_bar").find("li.stop").hide();
			if(instance1.start==0){
				instance1.start = instance1.slide_number-1;
			}
			else{
				instance1.start = instance1.start - 1;
			}
			set_slide("pre",instance1,instance1.start);
			
			//initLR(instance1);
		}
	});
	this.obj.find(".button_bar").find("li.stop").click(function(){
		instance1.obj.find("ul.button_bar").find("li.start").show();
		instance1.obj.find("ul.button_bar").find("li.stop").hide();
		clearInterval(instance1.initPro);
	});
	this.obj.find(".button_bar").find("li.start").click(function(){
		instance1.obj.find("ul.button_bar").find("li.start").hide();
		instance1.obj.find("ul.button_bar").find("li.stop").show();
		initLR(instance1);
	});
	this.obj.find(".button_bar").find("li.right").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			instance1.obj.find("ul.button_bar").find("li.start").show();
			instance1.obj.find("ul.button_bar").find("li.stop").hide();
			if(instance1.start==instance1.slide_number-1){
				instance1.start = 0;
			}
			else{
				instance1.start = instance1.start + 1;
			}
			set_slide("next",instance1,instance1.start);
			//initLR(instance1);
		}
	});
}
function slideTB(obj,autopPlay,duration,start,timeeffect,typeeffect,itemSpace){
	var current_slide = start;
	this.autopPlay = autopPlay;
	this.start = start;
	this.timeeffect = timeeffect;
	this.typeeffect = typeeffect;
	this.itemSpace  = itemSpace;
	if(isNaN(this.itemSpace)){this.itemSpace=0;}
	this.busy = false;
	this.initPro;
    this.interval = duration;
    this.obj = obj;
	this.width = obj.find(".preview_container").width();
	this.height = obj.find(".preview_container").height();
	this.leftPos = this.height + this.itemSpace;
	this.slide_number = obj.find("ul.item").find("li").length;
	this.Initcomponent = function(){
		if(this.slide_number>0){
			//this.obj.find(".preview").find("ul").append("<li>" + this.obj.find(".container").find("li:eq(0)").html() + "</li>");
		}
	};
	this.startSlide = function(){
        initLR(this);
    };
	function initLR(instance){
		instance.initPro = setInterval(function(){
			if(instance.busy==false){
				instance.busy=true;
				if(instance.start==0){
					instance.start = instance.slide_number - 1;
				}
				else{
					instance.start = instance.start - 1;
				}
				set_slide("pre",instance,instance.start);
			}
		},instance.interval);
	}
	function set_slide(type,instance,index){
		instance.obj.find("ul.navigator").find("li").removeAttr("class");
		instance.obj.find("ul.navigator").find("li:eq("+index.toString()+")").attr("class","active");
		var add_value = "<li>"+instance.obj.find("ul.item").find("li:eq("+index.toString()+")").html()+"</li>";
		if(type=="pre"){
			instance.obj.find("ul.preview").append(add_value);
			instance.obj.find("ul.preview").stop().animate({
                top: "-"+instance.leftPos.toString()+"px"
            },instance.timeeffect,instance.typeeffect,function(){
                instance.obj.find("ul.preview").find("li:eq(0)").remove();
                instance.obj.find("ul.preview").css("top","0px");
				instance.busy=false;
				//initLR(instance);
            });
		}
		else{
			instance.obj.find("ul.preview").prepend(add_value);
            instance.obj.find("ul.preview").css("top","-"+instance.leftPos.toString()+"px");
            instance.obj.find("ul.preview").stop().animate({
                top: "0px"
            },instance.timeeffect,instance.typeeffect,function(){
                instance.obj.find("ul.preview").find("li:eq(1)").remove();
				instance.busy=false;
				//initLR(instance);
            });
		}
	}
	if(this.autopPlay==true){this.startSlide();}
	var instance1 = this;
	
	this.obj.find("ul.navigator").find("li").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			var index = instance1.obj.find("ul.navigator").find("li").index($(this));
			if(index>instance1.start){
				instance1.start = index;
				set_slide("pre",instance1,instance1.start);
			}
			if(index<instance1.start){
				instance1.start = index;
				set_slide("next",instance1,instance1.start);
			}
			initLR(instance1);
		}
	});
	
	this.obj.find(".button_bar").find("li.left").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			if(instance1.start==0){
				instance1.start = instance1.slide_number-1;
			}
			else{
				instance1.start = instance1.start - 1;
			}
			set_slide("pre",instance1,instance1.start);
			initLR(instance1);
		}
	});
	this.obj.find(".button_bar").find("li.right").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			if(instance1.start==instance1.slide_number-1){
				instance1.start = 0;
			}
			else{
				instance1.start = instance1.start + 1;
			}
			set_slide("next",instance1,instance1.start);
			initLR(instance1);
		}
	});
}
function slideLRItem(obj,autopPlay,duration,start,timeeffect,typeeffect,itemSpace){
	var current_slide = start;
	this.autopPlay = autopPlay;
	this.start = start;
	this.timeeffect = timeeffect;
	this.typeeffect = typeeffect;
	this.itemSpace  = itemSpace;
	if(isNaN(this.itemSpace)){this.itemSpace=0;}
	this.busy = false;
	this.initPro;
    this.interval = duration;
    this.obj = obj;
	this.width = obj.find(".preview_container").width();
	this.height = obj.find(".preview_container").height();
	this.leftPos = this.itemSpace;
	this.slide_number = obj.find("ul.item").find("li.itemLi").length;
	if(this.start>this.slide_number-1){this.start=0;}
	this.Initcomponent = function(){
		if(this.slide_number>0){
			//this.obj.find(".preview").find("ul").append("<li>" + this.obj.find(".container").find("li:eq(0)").html() + "</li>");
		}
	};
	this.startSlide = function(){
        initLR(this);
    };
	function initLR(instance){
		instance.initPro = setInterval(function(){
			if(instance.busy==false){
				instance.busy=true;
				if(instance.start==instance.slide_number-1){
					instance.start = 0;
				}
				else{
					instance.start = instance.start + 1;
				}
				set_slide("next",instance,instance.start);
				/*
				if(instance.start==0){
					instance.start = instance.slide_number - 1;
				}
				else{
					instance.start = instance.start - 1;
				}
				set_slide("pre",instance,instance.start);
				*/
			}
		},instance.interval);
	}
	function set_slide(type,instance,index){
		instance.obj.find("ul.navigator").find("li").removeAttr("class");
		instance.obj.find("ul.navigator").find("li:eq("+index.toString()+")").attr("class","active");
		var add_value = '<li class="itemLi">'+instance.obj.find("ul.item").find("li.itemLi:eq("+index.toString()+")").html()+'</li>';
		if(type=="next"){
			instance.obj.find("ul.preview").append(add_value);
			instance.obj.find("ul.preview").stop().animate({
                left: "-"+instance.leftPos.toString()+"px"
            },instance.timeeffect,instance.typeeffect,function(){
                instance.obj.find("ul.preview").find("li.itemLi:eq(0)").remove();
                instance.obj.find("ul.preview").css("left","0px");
				instance.busy=false;
				//initLR(instance);
            });
		}
		else{
			instance.obj.find("ul.preview").prepend(add_value);
            instance.obj.find("ul.preview").css("left","-"+instance.leftPos.toString()+"px");
            instance.obj.find("ul.preview").stop().animate({
                left: "0px"
            },instance.timeeffect,instance.typeeffect,function(){
                instance.obj.find("ul.preview").find("li.itemLi:eq(1)").remove();
				instance.busy=false;
				//initLR(instance);
            });
		}
	}
	if(this.autopPlay==true){this.startSlide();}
	var instance1 = this;
	
	this.obj.find("ul.navigator").find("li").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			var index = instance1.obj.find("ul.navigator").find("li").index($(this));
			if(index>instance1.start){
				instance1.start = index;
				set_slide("next",instance1,instance1.start);
			}
			if(index<instance1.start){
				instance1.start = index;
				set_slide("pre",instance1,instance1.start);
			}
			initLR(instance1);
		}
	});
	
	this.obj.find(".button_bar").find("li.left").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			instance1.obj.find("ul.button_bar").find("li.start").show();
			instance1.obj.find("ul.button_bar").find("li.stop").hide();
			if(instance1.start==0){
				instance1.start = instance1.slide_number-1;
			}
			else{
				instance1.start = instance1.start - 1;
			}
			set_slide("pre",instance1,instance1.start);
			
			//initLR(instance1);
		}
	});
	this.obj.find(".button_bar").find("li.stop").click(function(){
		instance1.obj.find("ul.button_bar").find("li.start").show();
		instance1.obj.find("ul.button_bar").find("li.stop").hide();
		clearInterval(instance1.initPro);
	});
	this.obj.find(".button_bar").find("li.start").click(function(){
		instance1.obj.find("ul.button_bar").find("li.start").hide();
		instance1.obj.find("ul.button_bar").find("li.stop").show();
		initLR(instance1);
	});
	this.obj.find(".button_bar").find("li.right").click(function(){
		if(instance1.busy==false){
			instance1.busy=true;
			clearInterval(instance1.initPro);
			instance1.obj.find("ul.button_bar").find("li.start").show();
			instance1.obj.find("ul.button_bar").find("li.stop").hide();
			if(instance1.start==instance1.slide_number-1){
				instance1.start = 0;
			}
			else{
				instance1.start = instance1.start + 1;
			}
			set_slide("next",instance1,instance1.start);
			//initLR(instance1);
		}
	});
}