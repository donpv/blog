var alert_select_timeout;var ajaxBusy = false;
var hover_on_info_hidden = false;
$(document).ready(function() {
	$("[rel='showsub']").click(function(){
		var obj = $(this);
		obj.parent().parent().find("ul").slideUp();
		obj.parent().find("ul").slideDown();
	});
	
	$(".info_hidden").hover(function(){
		hover_on_info_hidden = true;
	},
	function(){
		hover_on_info_hidden = false;
	});
	$("body").click(function(){
		if(hover_on_info_hidden==false){
			$(".info_hidden").hide();
		}
	});
	$("#changeShowroom").find("option").click(function(){
        var mark = $(this).attr("value");
        $('html, body').animate({
            scrollTop: $("#"+mark).offset().top
        }, 700,"linear",function(){
            
        });
    });
	
	$(".toshow").focus(function(){
		$(".info_hidden").css("display","block");
	});
	
	
    $(".viewShowroomImg").click(function(){
        var parent_obj = $(this).parent().parent().find(".showRoomPreviewW");
        parent_obj.fadeIn(300);
    });
    $(".closeShowRoom").click(function(){
        var parent_obj = $(this).parent().parent().parent();
        parent_obj.fadeOut(300);
    });
    $(".showRoomPreviewW").find("li.prevButton").click(function(){
        var parent_obj = $(this).parent().parent().parent().parent();
        var index = parent_obj.find(".showRoomImg").find("img").index(parent_obj.find(".showRoomImg").find(".Imgactive"));
        if(index>0){
            index = index - 1;
        }
        else{
            index = parent_obj.find(".showRoomImg").find("img").length-1;
        }
        parent_obj.find(".showRoomImg").find("img").removeAttr("class");
        parent_obj.find(".showRoomImg").find("img:eq("+index+")").attr("class","Imgactive");
    });
    $(".showRoomPreviewW").find("li.nextButton").click(function(){
        var parent_obj = $(this).parent().parent().parent().parent();
        var index = parent_obj.find(".showRoomImg").find("img").index(parent_obj.find(".showRoomImg").find(".Imgactive"));
        if(index<parent_obj.find(".showRoomImg").find("img").length-1){
            index = index + 1;
        }
        else{
            index = 0;
        }
        parent_obj.find(".showRoomImg").find("img").removeAttr("class");
        parent_obj.find(".showRoomImg").find("img:eq("+index+")").attr("class","Imgactive");
    });
    $(".otherVideoList").find("li.prevButton").click(function(){
        if(videoIndex>2){
            videoIndex = videoIndex - 1;
            var pos = $(".otherVideoList").find(".otherVideos").position().left;
            pos = pos + 268;
            $(".otherVideoList").find(".otherVideos").stop().animate({
                left: pos+"px"
            },500,"linear",function(){
                
            });
        }
    });
    $(".otherVideoList").find("li.nextButton").click(function(){
        if(videoIndex<$(".otherVideoList").find(".otherVideos").find(".story").length-1){
            videoIndex = videoIndex + 1;
            var pos = $(".otherVideoList").find(".otherVideos").position().left;
            pos = pos - 268;
            $(".otherVideoList").find(".otherVideos").stop().animate({
                left: pos+"px"
            },500,"linear",function(){
                
            });
        }
    });
	$("[name='check_yahoo_online']").each(function(index){
		var yahoo = $(this).attr("idata");
		var obj_online = $(this).find(".avatar_online");
		var obj_offline = $(this).find(".avatar_offline");
		var url = base_folder + "modules/check_yahoo_online.php?nick="+yahoo+"&"+Math.random();
		$.get(url,function(data){
			if(data=="offline"){
				obj_online.hide();
				obj_offline.show();
			}
		});
	});
	
	$(".tab_bar li").click(function(){
        var index = $(".tab_bar").find("li").index($(this));
        var current_index = $(".tab_bar").find("li").index($(".tab_bar").find("li.active"));
        if(current_index!=index){
            $(".tab_bar li").removeAttr("class");
            $(".tab_bar li:eq("+index+")").attr("class","active");
            $(".tab_content").find(".tab").hide();
            $(".tab_content").find(".tab:eq("+index+")").fadeIn(300);
        }
    });
	$("#lookup").live("click",function(){
		var gender = $("[name='gender']").val();
		var year = $("[name='year']").val();
		var reqData = "act=show_result&gender="+gender + "&year="+year;
		var request = jQuery.ajax({
			url: base_folder+"ajax/ajax.php",
			type: "POST",
			data: reqData,
			cache: false,
			success: function(result){				
				$("body").prepend(result);
				re_setup_scroll_homepage();
			}
		});
	});
	$(".closeWindows").live("click",function(){
		$(this).parents(".addAPinWindows").remove();
	});
	function re_setup_scroll_homepage(){
		$(".content_1").mCustomScrollbar({
			scrollButtons: {
				enable: true
			}
		});
	}
	/*
	$(".video_player").click(function(){
		$(".video_preview").slideToggle(800);
	});
	*/
	if($(".zoom_image").length>0){
		$('.zoom_image').jqzoom({zoomWidth:450,zoomHeight:300,imgHeight:350,imgWidth:300,xOffset:7,yOffset:0});
	}
	if($(".fancybox_image").length>0){
		$(".fancybox_image").fancybox();
	}
	$(".video_player").click(function(){
		$(".video_preview").slideToggle(800);
	});
	$("#view_slide_image a").click(function(){
        $(this).parent().find("a").removeAttr("class");
        $(this).attr("class","active");
        var index = $("#view_slide_image a").index($(this));
        var image = $(this).attr("idata");
        var str = '<a class="zoom_image" onclick="viewFancyImage($(this));" rel="gallery" href="'+image +'"><img class="image_news" src="'+image +'"></a>'
        
        $(".image_large").html(str);
        //$(".image_large").find("a:eq("+index.toString()+")").show();
        $('.zoom_image').jqzoom({zoomWidth:350,zoomHeight:300,imgHeight:350,imgWidth:300,xOffset:7,yOffset:0});
    });
	/*
	initCompare = setInterval(function(){
		if(ajaxBusy==false){
			ajaxBusy=true;
			getAjaxForCompare()
		}
	},1000);
	*/
});
isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
extraTopPos = 160;
$( window ).load(function() {
    checkPos($(window).width());
});
$(window).scroll(function(){
    wPos = $(window).scrollTop();
    move_obj(wPos);
});
$(window).resize(function() {
	if($( window ).width()<1000){
		$("body").css("overflow-x","auto");
	}
	else{
		$("body").css("overflow-x","hidden");
	}
    checkPos($(window).width());
});
function viewFancyImage(obj){
	var image = obj.attr("href");
    $(".fancybox_image").each(function(index){
        var idata = $(this).attr("href");
        if(image==idata){
            $(this).click();
        }
    });
}
function change_hyper_link(obj){
	var url = obj.attr("idata");
	window.open(url, '_blank');
	window.focus();
}
function change_price_of_product(obj){
	var price = obj.attr("idata");
	$("#price_of_select_size").html(addCommas(price));
}
function close_comment_form(){
	$("#comment_input").slideToggle(800);
	return false;
}
function logout_click(msg){
    if(confirm(msg)){
	try{
		$.cookie(rootuser,null,{ path:base_folder, domain:domain});
		$.cookie(rootpass,null,{ path:base_folder, domain:domain});
		$.cookie(member_user,null,{ path:base_folder, domain:domain});
		$.cookie(member_pass,null,{ path:base_folder, domain:domain});
		window.location = path_info;
		}
		catch(err){alert(err);}
    }
}
	function send_message_check(){
		try{
			if(document.frmSendmessage.fReceiver.value==''){
				document.frmSendmessage.fReceiver.style.border = '1px solid red';
				document.frmSendmessage.fReceiver.focus();
				document.getElementById('send_message_status').style.color = '#ff0000';
				document.getElementById('send_message_status').innerHTML = 'Vui lòng điền Tên người nhận';
				return false;
			}
			else{
				document.frmSendmessage.fReceiver.style.border = '1px solid #CCCCCC';
			}
			if(document.frmSendmessage.fTitle.value==''){
				document.frmSendmessage.fTitle.style.border = '1px solid red';
				document.frmSendmessage.fTitle.focus();
				document.getElementById('send_message_status').style.color = '#ff0000';
				document.getElementById('send_message_status').innerHTML = 'Vui lòng điền Tiêu đề';
				return false;
			}
			else{
				document.frmSendmessage.fTitle.style.border = '1px solid #CCCCCC';
			}
			if(document.frmSendmessage.fContent.value==''){
				document.frmSendmessage.fContent.style.border = '1px solid red';
				document.frmSendmessage.fContent.focus();
				document.getElementById('send_message_status').style.color = '#ff0000';
				document.getElementById('send_message_status').innerHTML = 'Vui lòng điền Nội dung';
				return false;
			}
			else{
				document.frmSendmessage.fContent.style.border = '1px solid #CCCCCC';
			}
			document.getElementById('send_message_status').style.color = '#000000';
			document.getElementById('send_message_status').innerHTML = 'Đang gửi đi... <img src="' + base_folder + 'admincp/media/loading2.gif" />';
			return true;
		}
		catch(err){}
	}
	function send_message_status(status,message){
		try{
			document.getElementById('send_message_status').innerHTML = message;
			if(status == '1'){
				document.getElementById('send_message_status').style.color = '#0359BB';
				document.frmSendmessage.fContent.value = '';
				document.frmSendmessage.fTitle.value = '';
			}
			else{
				document.getElementById('send_message_status').style.color = '#ff0000';
			}
			return true;
		}catch(err){
			//alert(err);
			return false;
		}
	}
	function delete_message(messageid){
        if (confirm('Bạn có chắc muốn xóa tin nhắn?')){
            try{
				show_alert_doing2();
				//document.getElementById("over_wrapper_process").style.display = 'block';
				if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function(){   
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						close_alert_doing2();
						if(xmlhttp.responseText=="1"){
							//document.getElementById("over_wrapper_process").style.display = 'none';
							alert("Xóa tin nhắn thành công!");
						}
						else{
							//document.getElementById("over_wrapper_process").style.display = 'none';
							alert(xmlhttp.responseText);
						}
					}                          
				}     
				xmlhttp.open("GET",base_folder + "modules/delete_message.php?messageid="+messageid,true);
				xmlhttp.send();
			}
			catch(err){
				//alert(err);
			}
        }
	}
	function user_up_level_submit(){
		document.getElementById("over_wrapper_process").style.display = 'block';
		return true;
	}
	function user_up_level_status(status,message){
		try{
			document.getElementById("over_wrapper_process").style.display = 'none';
			alert(message);
			if(status==1){
				window.location.reload();
			}
			return false;
		}
		catch(err){return false;}
	}
	function check_input_value(control,require){
		try{
			if(control.value != ''){
				control.style.background = '#FAFFBD';
				control.style.border = '1px solid #999999';
			}
			else{
				control.style.background = '#FFFFFF';
				if(require==1){control.style.border = '1px solid #ff0000';}
			}
			return true;
		}catch(err){alert(err);return false;}
	}
function user_login_show(){
	$("#windowLogin").fadeIn(300);
	$("#over_dark_transparent").show();
	$("#frmUserlogin").find("[name='fLogin_Username']").focus();
}
function hideLoginWindow(){
    $("#over_dark_transparent").hide();
    $("#windowLogin").fadeOut(500);
}
	function show_alert_for_select(message,timeoutnumber){
        try{
            alert_select_timeout_clear();
			$("#id_alert_select").html(message);
			$("#id_alert_select").fadeIn(300);
            alert_select_timeout = setTimeout("hide_alert_for_select()",timeoutnumber);
        }
        catch(err){alert(err);return false;}
    }
	function alert_select_timeout_clear(){
        clearTimeout(alert_select_timeout);
    }
	function hide_alert_for_select(){
        try{
			$("#id_alert_select").hide();
			$("#over_wrapper").hide();
			$("#id_play_help").hide();
        }
        catch(err){alert(err);return false;}
    }
function check_facebook_status(id,id2){
    var pic = document.getElementById(id);
    var des = document.getElementById(id2);
    if(pic.height>0){
        des.style.display = 'block';
        return true;
    }
    else{
        des.style.display = 'none';
        return false;
    }
}
function Delete_Cookie( name, path, domain ) {
        if ( Get_Cookie( name ) ) document.cookie = name + "=" +
                ( ( path ) ? ";path=" + path : "") +
                ( ( domain ) ? ";domain=" + domain : "" ) + ";expires=Thu, 01-Jan-2000 00:00:01 GMT";
    }
	$(document).ready(function(){
	
});
// Go top
function gotop(pos){
    $('html, body').animate({
        scrollTop: pos
    }, 500);
}
// Extra left and right
function checkPos(windowWidth){
    if(windowWidth < 1050) {
        $("#divAdLeft").hide();
        $('#divAdRight').hide();
	}else{
		 $("#divAdLeft").show();
		 $('#divAdRight').show();
		 if(extraTopPos+$('#divAdLeft').height()>$(window).height()){
			if($(window).height()>$('#divAdLeft').height()){
				extraTopPos = $(window).height() - $('#divAdLeft').height()+15	;				
			}
			else{
				extraTopPos = 0;
			}
		}
		 var width_of_extra_left = $("#divAdLeft").width();
		 var width_of_extra_right = $("#divAdRight").width();
		 posLeft = (windowWidth - 1100)/2 - width_of_extra_left + 1;
		 posRight = (windowWidth - 1100)/2 - width_of_extra_right + 1;
		 $("#divAdLeft").css({ top:extraTopPos, left: posLeft, position: "absolute" });
		 $("#divAdRight").css({ top:extraTopPos, right: posRight, position: "absolute" });
		 heightLeft = $('#divAdLeft').height();
		 if(heightLeft>$(window).height()+5){
			heightLeft = $(window).height()-5;
			$("#divAdLeft").css({ "height": heightLeft+"px","overflow": "hidden" });
		 }
		 heightRight = $('#divAdRight').height();
		 if(heightRight>$(window).height()+5){
			heightRight = $(window).height()-5;
			$("#divAdRight").css({ "height": heightRight+"px","overflow": "hidden" });
		 }
	}
}
function move_obj(wPos){
    wPos = wPos + extraTopPos;
    $('#divAdLeft').stop().animate({top:wPos+"px"},500,"linear");
    $('#divAdRight').stop().animate({top:wPos+"px"},500,"linear");
}
function show_alert_doing2(){
    $('#alert_doing_form2').show();
	//$('#alert_doing_form2').fadeIn(300);
}
function close_alert_doing2(){
    $('#alert_doing_form2').hide();
}
function customer_cart_process_status(status,message){
	alert(message);
	close_alert_doing2();
}

function customer_cart_submit(frm){
	strreturn = true;
	var obj_focus = 1;
	frm.find(".warning").hide();
	frm.find(".input_style").find("input").each(function(index){
		//$(this).css("border","1px solid #ABABAB");
		$(this).val($.trim($(this).val()));
		if($(this).attr("require")=="true"){
			if($(this).val()==$(this).attr("default")){
				if(obj_focus==1){obj_focus=$(this);}
				$(this).parent().find(".warning").fadeIn(500);
				//$(this).css("border","1px solid #ff0000");
				strreturn = false;
			}
		}
	});
	frm.find(".input_style").find("textarea").each(function(index){
		//$(this).css("border","1px solid #ABABAB");
		$(this).val($.trim($(this).val()));
		if($(this).attr("require")=="true"){
			if($(this).val()==$(this).attr("default")){
				//$(this).css("border","1px solid #ff0000");
				if(obj_focus==1){obj_focus=$(this);}
				$(this).parent().find(".warning").fadeIn(500);
				strreturn = false;
			}
		}
	});
	if(strreturn==false){
		if($("#captcha_image").length>0){
			document.getElementById("captcha_image").src=base_folder+"captcha/captcha.php?"+Math.random();
		}
		if(obj_focus!=1){obj_focus.focus();}
	}
	else{
		show_alert_doing2();
	}
	return strreturn;
}
function customer_cart_input(obj){
	$(".customer_info").show();
	obj.parent().hide();
	$('html, body').animate({
            scrollTop: $("#customer_info").offset().top
        }, 700);
	
	return false;
}


function add_to_cart(contentid,gotoCart){
	value="";vcolor='';vsize='';
	if($("[name='fColor']").length>0){
		vcolor = $("[name='fColor']").val();
	}
	if($("[name='fSize']").length>0){
		vsize = $("[name='fSize']").val();
	}
	number = $(".fBuyNumber").val();
	if(isNaN(number)){number=1;}
	if($.cookie("cartid")==null || $.cookie("cartid")==""){
		value = contentid+"-"+number+"-"+vcolor+"-"+vsize;
	}
	else{
		value = $.cookie("cartid");
		temp = ";" + value;
		if(temp.indexOf(";"+contentid+"-")>=0){
			alert("Sản phẩm đã có trong giỏ hàng!");
			//return false;
		}
		else{
			value = value+";"+contentid+"-"+number+"-"+vcolor+"-"+vsize;
		}
	}
	$.cookie("cartid",value,{ path: base_folder, domain: domain });
	if(gotoCart==true){
		window.location = base_folder + "cart.php?language_alias="+language_alias ;
	}
	else{
		alert("Thêm sản phẩm thành công!");
		window.location.reload();
	}
}

function change_price_of_product_in_cart(obj){
	var item = obj.parent().parent().parent().parent();
	var price = parseInt(obj.attr("idata"));
	item.find(".price_number").val(price);
	var price_in_format = addCommas(price) + " đ";
	item.find("p.price").html(price_in_format);
	var number = item.find(".number_of_product").val();
	var total_of_rows = number * price;
	var total_of_rows_in_format = addCommas(total_of_rows) + " đ";
	item.find(".sum_of_product").val(total_of_rows_in_format);
	item.find(".sum_price_number").val(total_of_rows);
	caculate_summary_price_in_cart();
	// Recaculate
	
}
function caculate_summary_price_in_cart(){
	var sum_of_all = 0;
	$(".sum_of_product").each(function(i){
		var item = $(this).parent().parent();
		var sum_of_item = parseInt(item.find(".sum_price_number").val());
		sum_of_all = sum_of_all + sum_of_item;
	});
	$("[name='fSummary_price_number']").val(sum_of_all);
	var format_sum_of_all = addCommas(sum_of_all);
	$("[name='fSummary_price']").val(format_sum_of_all);
}
function change_number_of_product_cart(obj){
	var item = obj.parent().parent();
	var price = parseInt(item.find(".price_number").val());
	var number = parseInt(obj.val());
	if(isNaN(number)){
		number = 0;
	}
	var old_sum = parseInt(item.find(".sum_price_number").val());
	var current_sum_of_all = parseInt($("[name='fSummary_price']").attr("idata"));
	var sum = price*number;
	var format_sum = addCommas(sum)+" VNĐ";
	item.find(".sum_of_product").val(format_sum);
	item.find(".sum_price_number").val(sum);
	caculate_summary_price_in_cart();
	
	// CHANGE COOKIE
	var cookie_value = '';
	var count = $("[name='fCount']").val();
	for(var i=0;i<count;i++){
		var contentid = $("[name=fContentid"+i.toString()+"]").val();
		var numberContent = $("[name=fNumber"+i.toString()+"]").val();
		var colorContent = $("[name=fColor"+i.toString()+"]").val();
		var sizeContent = $("[name=fSize_name"+i.toString()+"]").val();
		cookie_value = cookie_value + ";"+ contentid + "-" + numberContent + "-" + colorContent + "-" + sizeContent;
	}
	if(cookie_value.indexOf(";")==0){cookie_value = cookie_value.substring(1);}
	$.cookie("cartid",cookie_value,{ path: base_folder, domain: domain });
	
	//var sum_of_all = parseInt($("[name='fSummary_price_number']").val());
	//sum_of_all = sum_of_all - old_sum + sum;
	//var format_sum_of_all = "Tổng tiền: "+addCommas(sum_of_all)+" đ";
	//$("[name='fSummary_price']").val(format_sum_of_all);
	//$("[name='fSummary_price_number']").val(sum_of_all);
}

function marketing_plan_submit(frm){
	strreturn = true;
	frm.find("input").each(function(index){
		$(this).css("border","1px solid #cccccc");
		$(this).val($.trim($(this).val()));
		if($(this).attr("require")=="true"){
			if($(this).val()==$(this).attr("default")){
				$(this).css("border","1px solid #ff8c8c");
				strreturn = false;
			}
		}
	});
	frm.find("textarea").each(function(index){
		$(this).css("border","1px solid #cccccc");
		$(this).val($.trim($(this).val()));
		if($(this).attr("require")=="true"){
			if($(this).val()==$(this).attr("default")){
				$(this).css("border","1px solid #ff8c8c");
				strreturn = false;
			}
		}
	});
	if(document.frmMarketingplan.fAgree.checked==false){
		frm.find("#fLabelAgree").attr("class","label_error");
		strreturn = false;
	}
	else{
		frm.find("#fLabelAgree").attr("class","normal");
	}
	if(strreturn==false){
		//document.getElementById("captcha_image").src=base_folder+"captcha/captcha.php?"+Math.random();
	}
	else{
		show_alert_doing2();
	}
	return strreturn;
}
function marketing_plan_status(status,message){
    close_alert_doing2();
	alert(message);
	if(status==1){
		$("[name='frmMarketingplan']")[0].reset();
    }
}

function adv_register_submit(frm){
	strreturn = true;
	var obj_focus = 1;
	frm.find(".warning").hide();
	frm.find("input").each(function(index){
		//$(this).css("border","1px solid #ABABAB");
		$(this).val($.trim($(this).val()));
		if($(this).attr("require")=="true"){
			if($(this).val()==$(this).attr("default")){
				if(obj_focus==1){obj_focus=$(this);}
				$(this).parent().find(".warning").fadeIn(500);
				//$(this).css("border","1px solid #ff0000");
				strreturn = false;
			}
		}
	});
	frm.find("textarea").each(function(index){
		//$(this).css("border","1px solid #ABABAB");
		$(this).val($.trim($(this).val()));
		if($(this).attr("require")=="true"){
			if($(this).val()==$(this).attr("default")){
				//$(this).css("border","1px solid #ff0000");
				if(obj_focus==1){obj_focus=$(this);}
				$(this).parent().find(".warning").fadeIn(500);
				strreturn = false;
			}
		}
	});
	if(strreturn==false){
		if($("#captcha_image").length>0){
			document.getElementById("captcha_image").src=base_folder+"captcha/captcha.php?"+Math.random();
		}
		if(obj_focus!=1){obj_focus.focus();}
	}
	else{
		show_alert_doing2();
	}
	return strreturn;
}
function adv_register_status(status,message){
    close_alert_doing2();
	if($("#captcha_image").length>0){
		$("[name='security_code']").val("");
		document.getElementById("captcha_image").src=base_folder+"captcha/captcha.php?"+Math.random();
	}
	alert(message);
	if(status==1){
		$("[name='frmAdvRegister']")[0].reset();
    }
}

function number_keypress(obj,event){
    if (window.event)
    {
        keypressed = window.event.keyCode; //IE
    }
    else
    { 
        keypressed = event.which; //NON-IE, Standard
    }
    if (keypressed < 48 || keypressed > 57){
        if (keypressed == 8 || keypressed == 127 || keypressed == 44 || keypressed == 46 || keypressed == 13 || keypressed == 32 || keypressed == 0)
        {
            if(keypressed == 44){
                if(obj.value.indexOf(",")>=0){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        else{
            $(obj).parent().find(".warning").show();
            return false;
        }
    }
}
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}
function validateEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
		return false;
	}
	else{
		return true;
	}
}
function validateNumber(number) {
	number = number.toString().replace(/\$|\./g,'');
	if(isNaN(number)){
		return false;
	}
	else{
		return true;
	}
}

// $(document).ready(function(){
  // $(".top_children").click(function(){
    // var idata = $(this).attr("idata");
	// $('.middle[mid="'+idata+'"]').slideToggle("slow");
  // });
// });