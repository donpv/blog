
<nav id="main-menu-container" class="main-menu-container">
    <div class="main-menu-auto">
        <div class="main-menu-listing-left"></div>
        <div class="menu_toggle">MENU</div>
        <div id="main-menu-listing" class="main-menu-listing">
            <ul class="megamenu clearfix">

                <li class="m-li  m-li- mm-item1 nav_main" name="parent" idata="12">
                    <a href="{{route('index')}}" title=""><span
                                class="m-left"></span><span
                                class="m-center">Trang Chủ</span><span
                                class="m-right"></span></a><span class="space">&nbsp;</span>
                </li>
                @if($categories)
                    @foreach($categories as $item_cat)
                        <li class="m-li  m-li- mm-item2 nav_main" name="parent" idata="14">
                            <a href="{{route('category_slug',$item_cat->slug)}}" title="{{$item_cat->name}}"><span class="m-left"></span><span
                                        class="m-center">{{$item_cat->name}}</span><span
                                        class="m-right"></span></a><span class="space">&nbsp;</span>
                            <?php $cat = \App\Category::where('parent_id', $item_cat->id)->get() ?>
                            @if($cat)
                                <ul class="sub-menu">
                                    @foreach($cat as $item_subcat)
                                        <li><a href="{{route('category_slug',$item_subcat->slug)}}" title="{{$item_subcat->name}}"><span>{{$item_subcat->name}}</span></a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                @endif
                <li class="m-li  m-li- mm-item1 nav_main" name="parent" idata="12">
                    <a href="{{route('video')}}" title=""><span
                                class="m-left"></span><span
                                class="m-center">Video</span><span
                                class="m-right"></span></a><span class="space">&nbsp;</span>
                </li>
                <li class="m-li  m-li- mm-item1 nav_main" name="parent" idata="12">
                    <a href="{{route('images')}}" title=""><span
                                class="m-left"></span><span
                                class="m-center">Ảnh</span><span
                                class="m-right"></span></a><span class="space">&nbsp;</span>
                </li>
                <li class="m-li  m-li- mm-item1 nav_main" name="parent" idata="12">
                    <a href="{{route('contact')}}" title=""><span
                                class="m-left"></span><span
                                class="m-center">Liên hệ</span><span
                                class="m-right"></span></a><span class="space">&nbsp;</span>
                </li>
                <li class="m-li  m-li- mm-item1 nav_main" name="parent" idata="12">
                    <a href="{{route('contact')}}" title=""><span
                                class="m-left"></span><span
                                class="m-center">Tìm kiếm tra cứu</span><span
                                class="m-right"></span></a><span class="space">&nbsp;</span>
                </li>

            </ul>
        </div>
        <div class="main-menu-listing-right"></div>
    </div>
</nav>