<div class="hotnews-container clearfix">
    <div id="mixedSlider">
        <div class="MS-content">

            @if($slider)
                @foreach($slider as $item_slider)
                    <div class="item">
                        <div class="imgTitle">
                            <a href="{{$item_slider->link}}" target="_blank"> <h2 class="blogTitle">{{$item_slider->name}}</h2>
                                <img src="{{$item_slider->photo}}"
                                     alt=""/></a>

                        </div>
                        <p>{{$item_slider->discription}}
                        </p>

                    </div>
                @endforeach

            @endif

        </div>
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left"
                                       aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right"
                                        aria-hidden="true"></i></button>
        </div>
    </div>
</div>

