<div class="hotnews-container clearfix">
    <div class="title-hotnews">
        <span class="text"><a href="">Tiêu điểm</a></span>
        <span class="button-more" style="display: none;"><a href="">Xem thêm</a></span>
    </div>
    <style>
        .wrapper-left {
            /*max-width: 700px;*/
            margin: 0 auto;
        }

        ul.post-content {
            width: 100%;
            overflow: hidden;
            padding-left: 0;
            list-style: none;
        }

        ul.post-content li.item-post:first-child {
            width: 65%;
            float: left;
        }

        ul.post-content li.item-post:not(:first-child) {
            width: 35%;
            float: left;
            margin-bottom: 18px;
        }

        li.item-post figure.post-image {
            margin: 0;
            padding: 0 5px;
        }

        li.item-post figure.post-image img {
            width: 30%;
            float: left;
            margin-right: 5px;
        }

        li.item-post figure.post-image a {
            text-decoration: none;
            color: #222;
            font-family: Arial;
            font-weight: 400;
        }

        li.item-post h5.entry-title {
            font-size: 12px;
            margin: 0;
        }

        li.item-post figure.post-image:not(:first-child) {
            margin-bottom: 12px;
        }

        @media (min-width: 768px) {
            li.item-post:first-child h5.entry-title {
                margin: 0;
                font-size: 14px;
                line-height: 20px;
                margin-top: 5px;
            }

            li.item-post:first-child figure.post-image a {
                display: block;
                background: #eee;
                padding: 10px;
            }

            li.item-post:first-child figure.post-image img {
                width: 100%;
                margin-bottom: 5px;
                height: 300px;
            }
        }

        @media (max-width: 767px) {
            ul.post-content li.item-post {
                width: 100% !important;
                margin-bottom: 10px;
                padding-bottom: 10px;
                border-bottom: 1px solid #666;
            }

            li.item-post figure.post-image img {
                width: 30%;
                float: left;
                margin-right: 5px;
            }

            li.item-post h5.entry-title {
                margin: 0;
                font-size: 16px;
            }
        }
    </style>
    <div class="wrapper-left">
        @if($hot)
            <ul class="post-content">
                @foreach($hot as $item_hot)
                    <li class="item-post">
                        <article>
                            <figure class="post-image">
                                <a href="{{route('post_slug',$item_hot->slug)}}"
                                   title="{{$item_hot->title}}">
                                    <img src="{{$item_hot->feture}}"
                                         alt="" title="{{$item_hot->title}}"/>
                                    <div class="post-title">
                                        <h3 class="entry-title" itemprop="headline">
                                            <strong style="font-size: 14px;"> {{$item_hot->title}}</strong></h3>
                                    </div>
                                </a>
                            </figure>
                        </article>
                    </li>
                @endforeach

            </ul>
        @endif
    </div>
</div>