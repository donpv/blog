<header id="wrap-header" class="wrap-header">
    <div class="wrap-header-auto">
        <div class="wrap-header-content">
            <div class="wrap-header-bg">
                <div id="layout-top">
                    <div class="top-menu">
                        <div class="normal-menu">
                            <a href="#" target="_blank">Theo dõi
                                chúng tôi trên Youtube</a>
                        </div>
                        <!--normal-menu-->
                    </div>
                    <!--/top menu-->
                    <div class="right-box">
                        <div class="social-follow">
                        </div>
                        <div class="top-search">
                            <form action="{{route('search')}}" method="GET">
                                <input class="txtsearch" type="text" placeholder="Search..." value=""
                                       name="keyword">
                                <button class="btnsearch" type="submit">
                                    Tìm kiếm
                                </button>
                            </form>
                        </div>
                    </div>
                    <!--/right-box-->
                </div>
                <!--/layout top-->
                <div class="layout-header1">
                    <div class="header-banner">
                        <a href="">
                            <img src="" alt="" title="">
                        </a>
                    </div>
                </div>
                <!--/layout-header1-->
                @if($setting)
                    @foreach($setting as $item)
                        <div class="layout-header2">
                            <div class="layout-logo" style="margin:0; padding:0; float: left;    width: 50%;" ;>
                                <h1>
                                    <a href="/"
                                       title="Gà giống Dabaco"><img style="height: 120px;width: 100%"
                                                                    alt="Gà giống Dabaco"
                                                                    src="{{url('')}}/{{$item->logo}}"/></a>
                                </h1>
                            </div>
                            <div class="layout-sologan">
                                <a href="#"><img style="height: 120px;width: 100%"
                                                 src="{{url('')}}/{{$item->banner}}"
                                                 alt="Sign up for ThePoultrySite weekly newsletter"
                                                 title="Sign up for ThePoultrySite weekly newsletter"></a>
                            </div>
                        </div>
                    @endforeach
                @endif
            <!--/layout-header2-->
                <style type="text/css">
                    .active a {
                        color: red;
                    }
                </style>
                @include('news.partials.menu')
            </div>
        </div>
    </div>
</header>