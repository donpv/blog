<footer id="wrap-bottom-container" class="wrap-bottom-container">
    <div class="wrap-bottom-auto">
        <div class="wrap-bottom-content">
            <div class="wrap-bottom-content-left">&nbsp;</div>
            <div class="wrap-bottom-content-center">
                <div class="footerLinksBorder">

                </div>
                <!--links border-->
            </div>
            <div class="wrap-bottom-content-right">&nbsp;</div>
        </div>
    </div>
    <div class="wrap-tag-container">
        <div class="wrap-tag-auto">
            <div class="wrap-tag-content">

            </div>
        </div>
    </div>
    @if(isset($setting))
        @foreach($setting as $item)
        <div class="footerLogoTerms">
            <img src="{{url('')}}/{{$item->logo}}" alt="5M Publishing" title="5M Publishing"
                 style="padding-bottom: 20px;">
            <span style="color: rgb(55, 62, 77); font-family: helvetica, arial, 'lucida grande', sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap; background-color: rgb(246, 247, 248);">Bản quyền thuộc <a
                        href="{{route('index')}}"><b>{{$item->name}}</b></a></span>
            <br/>
            <strong>Địa chỉ</strong>: {{$item->address}}
            <br/>
            <strong>Điện Thoại</strong>: {{$item->phone}}
            <br/>
            <strong>Email</strong>: {{$item->email}}
        </div>
        <!--/footerLogoTerms-->
        <div id="deverloper" class="clearfix">
            <div class="deverloper-auto clearfix">
                {{--<img src="{{$item->logo}}" alt=""/>--}}
                <p>
                    © <strong>Power by lonthuyphuong.vn</strong>
                </p>
            </div>
        </div>
        @endforeach
    @endif
</footer>