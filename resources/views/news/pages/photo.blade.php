@extends('news.layout.layout')
@section('content')
    <style type="text/css">
        .clearfix:before,
        .clearfix:after {
            content: " ";
            display: table;
        }

        .clearfix:after {
            clear: both;
        }

        .clearfix {
            *zoom: 1;
        }

        .clr {
            clear: both;
        }
        div.catalog-in-home-center {
            display:none;
        }
        div.gallery-container {
            width: 100%; padding: 0; margin: 0; font-size: 12px;
        }

        div.gallery-auto {
            width: 805px;
            margin: 0 10px 0 10px;
        }

        div.gallery-content {
            width: 100%;
            padding: 0 10px 0 0;
        }

        div.gallery-content h2 {
            height: 30px;
            background-position: 10px 5px;
            border-left: 5px solid #5AB4E8;
            border-bottom: 1px solid #CFCFCF;
            line-height: 30px;
            padding-left: 5px;
            margin-bottom: 20px;
            margin-top: 10px;
        }
        div.item-image-box {
            float: left;
            width:250px;
            height: 250px;
            border: 5px solid #CFCFCF;
            margin: 0 5px 40px 0;
        }


        div.image-gallery img {
            width:250px;
            height: 250px;
        }

        div.title-image {
            margin-top: 15px;
            font-size: 14px;
            color: #222;
            font-weight: bold;
        }

        div.gallery-content h2 {
            text-decoration: none;
            font-size: 16px;
            font-weight: bold;
            color: #222;
        }

    </style>
    <div class="gallery-container">
        <div class="gallery-auto">
            <div class="gallery-content">
                <h2>Gallery Gà Giống Dabaco</h2>
                <div class="item-image-box">

                    <div class="image-gallery">
                        <a href="/anh/hop-tac/" title="Hợp tác"><img
                                    src="http://gagiongdabaco.com.vn/images/2016/12/23/0-hop-tac.jpg"></a></div>
                    <div class="title-image">
                        <a href="/anh/hop-tac/" title="Hợp tác">Hợp tác</a></div>
                </div>

            </div><!--/gallery-content-->
            <div class="clr"></div>
            <div class="">
            </div>
        </div><!--/gallery-auto-->
    </div>
@stop