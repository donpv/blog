@extends('news.layout.layout')

@section('content')
    <!-- Posts -->


@section('title')
 | {{ $cate->name}}
@endsection
<section >
    <!-- Category posts -->

    <div class="hotnews-container clearfix">
        <div class="title-hotnews">
            <span class="text"><a href="">{{$cate->name}}</a></span>
            <span class="button-more" style="display: none;"><a href="">Xem thêm</a></span>
        </div>
        <style>
            .wrapper-left {
                /*max-width: 700px;*/
                margin: 0 auto;
            }

            ul.post-content {
                width: 100%;
                overflow: hidden;
                padding-left: 0;
                list-style: none;
            }

            ul.post-content li.item-post:first-child {
                width: 65%;
                float: left;
            }

            ul.post-content li.item-post:not(:first-child) {
                width: 35%;
                float: left;
                margin-bottom: 18px;
            }

            li.item-post figure.post-image {
                margin: 0;
                padding: 0 5px;
            }

            li.item-post figure.post-image img {
                width: 30%;
                float: left;
                margin-right: 5px;
            }

            li.item-post figure.post-image a {
                text-decoration: none;
                color: #222;
                font-family: Arial;
                font-weight: 400;
            }

            li.item-post h5.entry-title {
                font-size: 12px;
                margin: 0;
            }

            li.item-post figure.post-image:not(:first-child) {
                margin-bottom: 12px;
            }

            @media (min-width: 768px) {
                li.item-post:first-child h5.entry-title {
                    margin: 0;
                    font-size: 14px;
                    line-height: 20px;
                    margin-top: 5px;
                }

                li.item-post:first-child figure.post-image a {
                    display: block;
                    background: #eee;
                    padding: 10px;
                }

                li.item-post:first-child figure.post-image img {
                    width: 100%;
                    margin-bottom: 5px;
                    height: 300px;
                }
            }

            @media (max-width: 767px) {
                ul.post-content li.item-post {
                    width: 100% !important;
                    margin-bottom: 10px;
                    padding-bottom: 10px;
                    border-bottom: 1px solid #666;
                }

                li.item-post figure.post-image img {
                    width: 30%;
                    float: left;
                    margin-right: 5px;
                }

                li.item-post h5.entry-title {
                    margin: 0;
                    font-size: 16px;
                }
            }
        </style>
        <div class="wrapper-left">
                @if($post)
                <ul class="post-content">
                    @foreach($post as $item_hot)
                        <li class="item-post">
                            <article>
                                <figure class="post-image">
                                    <a href="{{route('post_slug',$item_hot->slug)}}"
                                       title="{{$item_hot->title}}">
                                        <img src="{{url('')}}/{{$item_hot->feture}}"
                                             alt="" title="{{$item_hot->title}}"/>
                                        <div class="post-title">
                                            <h3 class="entry-title" itemprop="headline">
                                                <strong style="font-size: 14px;"> {{$item_hot->title}}</strong></h3>
                                        </div>
                                    </a>
                                </figure>
                            </article>
                        </li>
                    @endforeach

                </ul>
            @endif
        </div>
    </div>

    @if($sub_cate)
        @foreach($sub_cate as $item_sub_cate)

            <div class="column2">
                <section class="content_news_Category">
                    <h2>
                        <a href="#">{{$item_sub_cate->name}}</a></h2>
                    <header>
                        <header>
                            <?php $post_top = \App\Post::where('category_id', $item_sub_cate->id)->limit(1)->get() ?>
                            @foreach($post_top as $item_post_top)
                                <h4 class="bordernone"><img
                                            src="{{url('')}}/{{$item_post_top->feture}}">
                                </h4>
                                <h4><a itemprop="title"
                                       href="{{route('post_slug',$item_post_top->slug)}}">{{$item_post_top->title}}</a>
                                </h4>
                            @endforeach
                            <?php $post = \App\Post::where('category_id', $item_sub_cate->id)->skip(1)->take(3)->get(); ?>

                            @foreach($post as $item_post)
                                <h4><a itemprop="title"
                                       href="{{route('post_slug',$item_post->slug)}}">{{$item_post->title}}</a></h4>
                            @endforeach
                        </header>
                    </header>
                </section>
            </div>

    @endforeach



@endif

<!-- End Category posts -->
</section>
{{--@else--}}
{{--<section class="row">--}}
{{--<h4 class="cat-title mb25">Chuyên Mục {{ $key}}</h4>--}}
{{--<article class="post ten column">--}}
{{--<h3>Không có bài viết nào được tìm thấy.</h3>--}}
{{--</article>--}}
{{--</section>--}}
{{--@section('title')--}}
{{--| Không có bài viết--}}
{{--@endsection--}}
{{--@endif--}}
@stop
