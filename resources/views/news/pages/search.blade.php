@extends('news.layout.layout')
@section('title')
| Tìm kiếm bài viết
@endsection
@section('content')
<!-- Posts -->

<style>
	div.item-article {
		border-bottom: 1px solid #CFCFCF;
		padding: 10px 0;
		font-size: 12px;
		color: #6D6E71;
		line-height: 17px;
		font-family: arial, helvetical, sans-serif;
		overflow: hidden;
	}
	div.item-article div.image-product {
		float: left;
		margin-right: 5px;
	}
	div.item-article {
		border-bottom: 1px solid #CFCFCF;
		padding: 10px 0;
		font-size: 12px;
		color: #6D6E71;
		line-height: 17px;
		font-family: arial, helvetical, sans-serif;
		overflow: hidden;
	}
	div.item-article div.image-product img {
		width: 150px;
		height: 150px;
	}
	div.item-article span.title-article a {
		color: #333;
		text-decoration: none;
		font-weight: bold;
		font-size: 14px;
		line-height: 17px;
	}
</style>
<h4 class="cat-title mb25">Từ Khóa {{ $key }}</h4>
<section >
	@if(count($posts)==0)
	<article class="post ten column">
		<h3>Không có bài viết nào được tìm thấy.</h3>
	</article>
	@endif

	@foreach($posts as $post)

			<div class="item-article">
				<div class="image-product">
					<a href="{{route('post_slug',$post->slug)}}" title="{{$post->title}}"><img src="{{url('')}}/{{$post->feture}}" alt="{{$post->title}}" title="{{$post->title}}"></a>			</div>
				<span class="title-article">
				<a href="{{route('post_slug',$post->slug)}}" title="{{$post->title}}">{{$post->title}}</a>			</span>
				<p class="description">
					{{$post->discription}}</p>
				<span class="author"><a href="author/{{$post->admin->name}}">Người viết : {{$post->admin->name}}</a></span>
				<span class="date"><a href="#">{{date('G:i d-m-Y', strtotime($post->created_at)) }}</a></span>
			</div>

	@endforeach

</section>
@endsection