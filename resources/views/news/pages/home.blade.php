@extends('news.layout.layout')
@section('title','Trang chủ')
@section('content')
    <div class="catalog-in-home-left">
        <script text="text/javascript">
            $(document).ready(function () {
                $('.text-item').hover(function () {
                    $('.images-hotnews').hide();
                    var content_show = $(this).attr('name');
                    $('#' + content_show).show();
                    $('.active').removeClass('active');
                    $(this).addClass('active');
                });

            });
        </script>
    @include('news.partials.topmain')
    <!--/hotnews-->

        @include('news.partials.slide')
        @if($cates)
            @foreach($cates as $cate)
                <div class="column2">
                    <section class="content_news_Category">
                        <h2>
                            <a href="{{route('category_slug',$cate->slug)}}">{{$cate->name}}</a></h2>
                        <header>
                            <header>
                                <?php $post_top = \App\Post::where('category_id', $cate->id)->limit(1)->get() ?>
                                @foreach($post_top as $item_post_top)
                                    <h4 class="bordernone"><img
                                                src="{{$item_post_top->feture}}">
                                    </h4>
                                    <h4><a itemprop="title"
                                           href="{{route('post_slug',$item_post_top->slug)}}">{{$item_post_top->title}}</a>
                                    </h4>
                                @endforeach
                                <?php $post = \App\Post::where('category_id', $cate->id)->skip(1)->take(3)->get(); ?>

                                @foreach($post as $item_post)
                                    <h4><a itemprop="title"
                                           href="{{route('post_slug',$item_post->slug)}}">{{$item_post->title}}</a></h4>
                                @endforeach
                            </header>
                        </header>
                    </section>
                </div>
        @endforeach
    @endif
    <!--/col1-->


    </div>
@endsection
