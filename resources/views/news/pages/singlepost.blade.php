@extends('news.layout.layout')
@section('content')
   @if($post)
@section('title')
    | {{ $post->title }}
@endsection


<div id="wrap-middle-container" class="wrap-middle-container">
    <div class="wrap-middle-auto clearfix">
        <div class="wrap-middle-content clearfix">
            <!--START Wrap Middle center-->
            <div class="wrap-middle-center">

                <div class="catalog-in-home">
                    <div class="catalog-in-home-left">

                        <style>
                            .clr {
                                clear: both;
                            }

                            div.articles1 {
                                float: left;
                                width: 74%;
                                margin-right: 5px;
                                padding: 0 5px;
                            }

                            h2.title-article {
                                height: 30px;
                                background: url(../images/Industry_News.jpg) no-repeat;
                                background-position: 10px 5px;
                                font-size: 16px;
                                font-weight: bold;
                                color: #0E884E;
                                border-left: 5px solid #0E884E;
                                border-bottom: 1px solid #CFCFCF;
                                line-height: 30px;
                                padding-left: 35px;
                                margin-bottom: 20px;
                                margin-top: 20px;
                                text-transform: uppercase;
                            }

                            div.articlestop {
                                float: left;
                                width: 575px;
                                position: relative;
                            }

                            span#titlebottom {
                                position: absolute;
                                bottom: 0;
                            }

                            div.newsArticesTitleImage {
                                font-size: 17px;
                                font-weight: bold;
                                line-height: 19px;
                                color: #333;
                                margin: 0;
                                padding-bottom: 5px;
                                width: 300px \9;
                            }

                            span.newsdate {
                                font-size: 10px;
                                color: #5DB1E3;
                            }

                            p.description {
                                font-size: 14px;
                                line-height: 22px;
                                color: #333;
                                margin: 0;
                                padding: 10px 0;
                                text-align: justify;
                                padding-right: 50px;

                                font-weight: bold;
                            }

                            div.detail-article p {
                                font-size: 14px;
                                line-height: 22px;
                                color: #333;
                                margin: 0;
                                padding: 10px 0;
                                text-align: left;
                                padding-right: 50px;
                                margin-left: 32px;
                                text-align: justify;
                            }

                            div.bottom-detail h3 {
                                text-shadow: 1px 1px 1px #FFF;
                                color: #404041;
                            }

                            div.dividerline {
                                width: 100%;
                                border-top: solid 1px #CFCFCF;
                                padding-bottom: 20px;
                            }

                            div.canyoulike div.header-like {
                                font-size: 1.1em;
                                color: #333333;
                                margin-bottom: 10px;
                                float: left;
                                font-weight: 600;
                                padding-bottom: 3px;
                            }

                            div.links-container ul.ul_container {
                                color: #404041;
                                font-size: 13px;
                                font-family: arial, serif;
                                margin-left: 32px;
                                clear: left;
                            }

                            div.links-container ul.ul_container li {
                                display: list-item;
                                text-align: -webkit-match-parent;
                                padding: 2px;
                            }

                            div.links-container ul.ul_container li a:link, div.links-container ul.ul_container li a:visited {
                                color: #2e9fff;
                                text-decoration: none;
                            }

                            div.links-container ul.ul_container li a:active, div.links-container ul.ul_container li a:hover {
                                text-decoration: underline;
                            }

                            div.column3 {
                                float: left;
                                width: 25%;
                                margin-right: 5px;
                                padding: 0 5px;
                            }

                            div.related-news h2.titleRelated {
                                height: 30px;
                                font-size: 16px;
                                font-weight: bold;
                                border-left: 5px solid #0E884E;
                                border-bottom: 1px solid #CFCFCF;
                                line-height: 30px;
                                padding-left: 10px;
                                margin-bottom: 20px;
                                margin-top: 20px;
                                color: #0E884E;
                            }

                            div.relatednews-item {
                                margin-bottom: 10px;
                            }

                            div.relatednews-item h3 {
                                font-size: 14px;
                                font-weight: bold;
                                line-height: 17px;
                                color: #333;
                                margin: 0;
                                padding-bottom: 5px;
                            }

                            div.relatednews-item h3 a:link, div.relatednews-item h3 a:visited {
                                color: #333;
                                font-size: 14px;
                                font-weight: bold;
                                text-decoration: none;
                            }

                            div.relatednews-item h3 a:hover, div.relatednews-item h3 a:active {
                                color: #c00;
                                text-decoration: none;
                            }

                            div.flagbar {
                                border-top: 1px solid #CFCFCF;
                                border-bottom: 1px solid #CFCFCF;
                                padding: 5px 0;
                                font-size: 10px;
                                margin-bottom: 10px;
                                color: #A7A9AC;
                            }

                            .bluetext {
                                color: #5DB1E3;
                            }

                            div.mostviews h2.titleMostviews {
                                height: 30px;
                                font-size: 16px;
                                font-weight: bold;
                                border-left: 5px solid #0E884E;
                                border-bottom: 1px solid #CFCFCF;
                                line-height: 30px;
                                padding-left: 10px;
                                margin-bottom: 20px;
                                margin-top: 20px;
                                color: #0E884E;
                            }

                            div.mostviews ol {
                                list-style: none;
                                font-weight: bold;
                            }

                            div.mostviews ol li {
                                font-weight: bold;
                                font-size: 12px;
                                padding-bottom: 6px;
                                margin-left: -40px;
                            }

                            div.mostviews ol li a:link, div.mostviews ol li a:visited {
                                color: #333;
                                text-decoration: none;
                                line-height: 17px;
                            }

                            div.mostviews ol li a:hover, div.mostviews ol li a:active {
                                color: #c00;
                                text-decoration: none;
                            }

                            h3.author-news {
                                font-style: italic;
                            }

                            div.detail-article {
                                font-size: 14px;
                                font-weight: normal;
                                font-family: arial, helvetica, sans-serif;
                                line-height: 24px;
                                text-align: justify;
                            }
                        </style>

                        <div class="articles1">
                            <h2 class="title-article">
                                tin tức
                                <div class="social-share" style="float: right;">
                                    <script>(function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                    <div class="fb-like fb_iframe_widget"
                                         data-href="{{route('post_slug',$post->slug)}}"
                                         data-layout="button_count" data-action="like" data-show-faces="true"
                                         data-share="true" fb-xfbml-state="rendered"
                                         fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=103&amp;href={{route('post_slug',$post->slug)}}&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=true">
                                        <span style="vertical-align: bottom; width: 134px; height: 20px;"><iframe
                                                    name="f2515556ece4c5" width="1000px" height="1000px" frameborder="0"
                                                    allowtransparency="true" allowfullscreen="true" scrolling="no"
                                                    allow="encrypted-media" title="fb:like Facebook Social Plugin"
                                                    src="https://www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=&amp;channel={{route('post_slug',$post->slug)}}&amp;container_width=103&amp;href={{route('post_slug',$post->slug)}}&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=true"
                                                    style="border: none; visibility: visible; width: 134px; height: 20px;"
                                                    class=""></iframe></span></div>
                                    <iframe id="twitter-widget-0" scrolling="no" frameborder="0"
                                            allowtransparency="true"
                                            class="twitter-share-button twitter-share-button-rendered twitter-tweet-button"
                                            style="position: static; visibility: visible; width: 61px; height: 20px;"
                                            title="Twitter Tweet Button"
                                            src="https://platform.twitter.com/widgets/tweet_button.c5b006ac082bc92aa829181b9ce63af1.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer={{route('post_slug',$post->slug)}}&amp;size=m&amp;text=Th%E1%BB%A7%20t%C6%B0%E1%BB%9Bng%3A%20Mu%E1%BB%91n%20ph%C3%A1t%20tri%E1%BB%83n%2C%20nu%C3%B4i%20con%20g%C3%A0%20con%20l%E1%BB%A3n%20c%C5%A9ng%20ph%E1%BA%A3i%20%C4%91%C4%83ng%20k%C3%BD&amp;time=1529221945150&amp;type=share&amp;url={{route('post_slug',$post->slug)}}"
                                            data-url="{{route('post_slug',$post->slug)}}"></iframe>
                                    <script>!function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0],
                                                p = /^http:/.test(d.location) ? 'http' : 'https';
                                            if (!d.getElementById(id)) {
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = p + '://platform.twitter.com/widgets.js';
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }
                                        }(document, 'script', 'twitter-wjs');</script>
                                    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"
                                            gapi_processed="true"></script>
                                    <div id="___plusone_0"
                                         style="text-indent: 0px; margin: 0px; padding: 0px; background: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 38px; height: 24px;">
                                        <iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0"
                                                marginwidth="0" scrolling="no"
                                                style="position: static; top: 0px; width: 38px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 24px;"
                                                tabindex="0" vspace="0" width="100%" id="I0_1529221944957"
                                                name="I0_1529221944957"
                                                src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;origin=http%3A%2F%2Fgagiongdabaco.com.vn&amp;url=http%3A%2F%2Fgagiongdabaco.com.vn%2Fthu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.vi.FLYT8IScbFQ.O%2Fm%3D__features__%2Fam%3DQQE%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCOXNHsxtHd0PnY4WFlbPHe08qYrqQ#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh&amp;id=I0_1529221944957&amp;_gfid=I0_1529221944957&amp;parent=http%3A%2F%2Fgagiongdabaco.com.vn&amp;pfname=&amp;rpctoken=73273919"
                                                data-gapiattached="true" title="G+"></iframe>
                                    </div>
                                </div>
                            </h2>

                                <div class="articlestop">
                                    <img src="{{url($post->feture)}}" alt=""
                                         style="width:282px; height: 213px; float: left; margin-right: 10px; border: 1px solid #cfcfcf;">

                                    <span id="titlebottom">
			<h2 class="newsArticlesTitleImage">{{$post->title}}</h2>
			<span class="newsdate">{{$post->created_at}}</span>
		</span>
                                </div><!--/articlestop-->
                                <div class="clr"></div>
                                <p class="description">
                                    <strong><em>{!! $post->description !!}</em></strong></p>
                                {!! $post->content !!}
                                <div class="bottom-detail"
                                     style="border-top: 1px solid #cfcfcf; border-bottom: 1px solid #cfcfcf; width: 100%; height: 65px; padding: 5px 0 5px 0;">

                                    <div style="float:left;margin:20px 10px 0 10px;"><h3 class="author_news">Nguồn: Sưu
                                            tầm</h3>
                                    </div>
                                </div>
                                <div class="bottom-social-share">
                                    <script>(function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                    <div class="fb-like fb_iframe_widget"
                                         data-href="http://gagiongdabaco.com.vn/thu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm"
                                         data-layout="button_count" data-action="like" data-show-faces="true"
                                         data-share="true" fb-xfbml-state="rendered"
                                         fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=575&amp;href=http%3A%2F%2Fgagiongdabaco.com.vn%2Fthu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=true">
                                        <span style="vertical-align: bottom; width: 134px; height: 20px;"><iframe
                                                    name="f18d7459da353b4" width="1000px" height="1000px"
                                                    frameborder="0" allowtransparency="true" allowfullscreen="true"
                                                    scrolling="no" allow="encrypted-media"
                                                    title="fb:like Facebook Social Plugin"
                                                    src="https://www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FmAiQUwlReIP.js%3Fversion%3D42%23cb%3Df2e64611bbaac88%26domain%3Dgagiongdabaco.com.vn%26origin%3Dhttp%253A%252F%252Fgagiongdabaco.com.vn%252Ff19b83243cb6c08%26relation%3Dparent.parent&amp;container_width=575&amp;href=http%3A%2F%2Fgagiongdabaco.com.vn%2Fthu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=true"
                                                    style="border: none; visibility: visible; width: 134px; height: 20px;"
                                                    class=""></iframe></span></div>
                                    <iframe id="twitter-widget-1" scrolling="no" frameborder="0"
                                            allowtransparency="true"
                                            class="twitter-share-button twitter-share-button-rendered twitter-tweet-button"
                                            style="position: static; visibility: visible; width: 61px; height: 20px;"
                                            title="Twitter Tweet Button"
                                            src="https://platform.twitter.com/widgets/tweet_button.c5b006ac082bc92aa829181b9ce63af1.en.html#dnt=false&amp;id=twitter-widget-1&amp;lang=en&amp;original_referer=http%3A%2F%2Fgagiongdabaco.com.vn%2Fthu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm&amp;size=m&amp;text=Th%E1%BB%A7%20t%C6%B0%E1%BB%9Bng%3A%20Mu%E1%BB%91n%20ph%C3%A1t%20tri%E1%BB%83n%2C%20nu%C3%B4i%20con%20g%C3%A0%20con%20l%E1%BB%A3n%20c%C5%A9ng%20ph%E1%BA%A3i%20%C4%91%C4%83ng%20k%C3%BD&amp;time=1529221945152&amp;type=share&amp;url=http%3A%2F%2Fgagiongdabaco.com.vn%2Fthu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm"
                                            data-url="http://gagiongdabaco.com.vn/thu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm"></iframe>
                                    <script>!function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0],
                                                p = /^http:/.test(d.location) ? 'http' : 'https';
                                            if (!d.getElementById(id)) {
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = p + '://platform.twitter.com/widgets.js';
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }
                                        }(document, 'script', 'twitter-wjs');</script>
                                    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"
                                            gapi_processed="true"></script>
                                    <div id="___plusone_1"
                                         style="text-indent: 0px; margin: 0px; padding: 0px; background: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 38px; height: 24px;">
                                        <iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0"
                                                marginwidth="0" scrolling="no"
                                                style="position: static; top: 0px; width: 38px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 24px;"
                                                tabindex="0" vspace="0" width="100%" id="I0_1529221945037"
                                                name="I0_1529221945037"
                                                src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;origin=http%3A%2F%2Fgagiongdabaco.com.vn&amp;url=http%3A%2F%2Fgagiongdabaco.com.vn%2Fthu-tuong-muon-phat-trien-nuoi-con-ga-con-lon-cung-phai-dang-ky-378.htm&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.vi.FLYT8IScbFQ.O%2Fm%3D__features__%2Fam%3DQQE%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCOXNHsxtHd0PnY4WFlbPHe08qYrqQ#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh&amp;id=I0_1529221945037&amp;_gfid=I0_1529221945037&amp;parent=http%3A%2F%2Fgagiongdabaco.com.vn&amp;pfname=&amp;rpctoken=30825668"
                                                data-gapiattached="true" title="G+"></iframe>
                                    </div>
                                </div>
                                <div class="dividerline"></div>
                                <div class="canyoulike">
                                    <div class="header-like">
                                        Có thể bạn quan tâm
                                    </div>
                                    <div class="links-container">
                                        <ul class="ul_container">

                                        </ul>
                                    </div>
                                </div>
                        </div><!--/detail article-->
                        <div class="column3">
                            <div class="related-news">
                                <h2 class="titleRelated" style="border-color:#0E884E;">Tin liên quan</h2>
                                @if($post_lq)
                                    @foreach($post_lq as $item_lq)
                                        <div class="relatednews-item">
                                            <h3>
                                                <a href="{{route('post_slug',$item_lq->slug)}}"
                                                   title="{{$item_lq->title}}">{{$item_lq->title}}</a></h3>
                                            <div class="flagbar">
                                                <span class="bluetext">{{$item_lq->created_at}}</span>
                                            </div>
                                        </div><!--item-relative-->
                                    @endforeach
                                @endif


                                <div class="dividerline" style="border-color:#0E884E;"></div>
                            </div>
                        </div>
                    </div>
                    @endif



                </div>

            </div>

            <!--END Wrap Middle center-->
        </div>
    </div>
</div>

@endsection