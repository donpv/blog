<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="background:#fff;background-attachment:fixed;">

<head>

    <title>@yield('title')</title>

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="canonical" href=""/>
    <link rel="icon" type="image/x-icon" href=""/>
    <link rel="stylesheet" type="text/css" href="{{asset('news/style/global_style.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('news/style/homepage.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('news/style/htmlstyle.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('news/style/home1.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('news/style/responsive.css')}}"/>
    <script type="text/javascript" src="{{asset('news/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/jquery.cookie.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/123_slide.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/123_htmlstyle_js.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/jquery.bxslider.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/123_main.js')}}"></script>
    <script type="text/javascript" src="{{asset('news/js/123_global.js')}}"></script>
</head>

<body>
<div id="fb-root"></div>
<div id="wrap">

    <div id="wrap-content-auto" class="clearfix">
        <div id="wrap-content">


            <div id="alert_doing_form2">
                <div class="alertShowing">&nbsp;<span class="message">Đang thực hiện</span> <img align="absMiddle"
                                                                                                 src="{{url('admincp/media/loading2.gif')}}"
                                                                                                 alt=""/></div>
            </div>
            <div id="alert_over_wrap2"></div>
            <div id="over_dark_transparent"></div>
            <div id="over_wrapper"></div>
            <!--Đoạn code đưa form login vào-->
            @include('news.partials.header')
            <section id="homepage">
                <div class="wrap-top-container">
                </div>
                <div id="wrap-middle-container" class="wrap-middle-container">
                    <div class="wrap-middle-auto clearfix">
                        <div class="wrap-middle-content clearfix">
                            <!--START Wrap Middle center-->
                            <div class="wrap-middle-center">
                                <div class="catalog-in-home">
                                    @yield('content')
                                </div>
                                <!--END Wrap Middle center-->
                            </div>
                        </div>
                    </div>
                    <div class="column1">
                        <div class="container-list">
                            <div class="images-title title-list">
                                <span class="text"><a href="/mon-an-tu-ga/" title="Món ăn từ gà">Món ăn từ gà</a></span>
                                <span class="button-more"><a href="/mon-an-tu-ga/"
                                                             title="Món ăn từ gà">Xem thêm</a></span>
                                <!-- <span class="text"><a href="">Món ăn từ gà</a></span>
                           <span class="button-more"><a href="">Xem thêm</a></span> -->
                            </div>
                            <div class="content-list">
                                <h3 class="title-text"><a
                                            style="text-decoration: none; color:#333; font-weight:bold; font-size:14px;  height:30px; margin-bottom:10px;  display: block;"
                                            href="/cach-luoc-ga-vang-ong-khong-bi-nut-de-cung-363.htm"
                                            title="Cách luộc gà vàng óng, không bị nứt để cúng">Cách luộc gà vàng óng,
                                        không
                                        bị nứt để cúng</a></h3>
                                <div class="images">
                                    <a href="">
                                        <a href="/cach-luoc-ga-vang-ong-khong-bi-nut-de-cung-363.htm"
                                           title="Cách luộc gà vàng óng, không bị nứt để cúng"><img
                                                    src="http://gagiongdabaco.com.vn/images/2018/03/05/Cach-luoc-ga-cung-khong-bi-nut-vang-ong-dang-dep-canh-tien-1-1519718544-543-width480height341.jpg"
                                                    alt="" title=""
                                                    style="margin-bottom: 10px; width: 280px; height: 225px"/></a>
                                    </a>
                                </div>
                            </div>
                            <div class="bottom-list"></div>
                        </div>
                        <!--/article with datetime-->
                    </div>
                    <!--/col1.2-->
                    <div class="column2">
                        <div class="container-list">
                            <div class="images-title title-list">
                                    <span class="text">
                        <a href="/anh" title="Ảnh">Ảnh</a>                     </span>
                                <span class="button-more">
                        <a href="/anh" title="Ảnh">Xem thêm</a>                        </span>
                            </div>
                            <div class="content-list">
                                <h3 class="title-text"><a
                                            style="text-decoration: none; color:#333; font-weight:bold; font-size:14px;  height:30px; margin-bottom:10px; display: block;"
                                            href="/ga-tan-ho-1-ngay-tuoi-298.htm" title="Gà Tân Hồ 1 ngày tuổi">Gà Tân
                                        Hồ 1
                                        ngày tuổi</a></h3>
                                <div class="images">
                                    <a href="">
                                        <a href="/ga-tan-ho-1-ngay-tuoi-298.htm" title="Gà Tân Hồ 1 ngày tuổi"><img
                                                    src="http://gagiongdabaco.com.vn/images/2016/09/20/tanho.jpg" alt=""
                                                    title="" style="margin-bottom: 10px; width: 280px; height: 225px"/></a>
                                    </a>
                                </div>
                            </div>
                            <div class="bottom-list"></div>
                        </div>
                        <!--/article with datetime-->
                    </div>
                    <!--/col2.2-->
                    <div class="column2">
                        <div class="container-list">
                            <div class="images-title title-list">
                                    <span class="text">
                        VIDEO NỔI BẬT
                        </span>
                            </div>
                            <div class="content-list">
                                <h3 class="title-text"><span
                                            style="text-decoration: none; color:#333; font-weight:bold; font-size:14px;  height:30px; margin-bottom:10px; display: block;">Video xem nhiều nhất</span>
                                </h3>
                                <div class="feature_video video-container" style="border: 1px solid #222;">
                                    <div style="text-align: center;">
                                        <iframe allowfullscreen="" frameborder="0" height="220"
                                                src="https://www.youtube.com/embed/UeyLEbD6afk" width="220"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-list"></div>
                        </div>
                        <!--/article with datetime-->
                    </div>
                    <!--/col2.2-->
                </div>
            </section>
            <div class="clr"></div>
            <div class="footer-adv-container">
                <div class="footer-adv-auto">
                    <div class="footer-adv-content"></div>
                </div>
            </div>
            <nav id="wrap-menu-bottom-container" class="wrap-menu-bottom-container">
                <div class="wrap-menu-bottom-auto">
                    <div class="wrap-menu-bottom-listing-left">&nbsp;</div>
                    <div id="wrap-menu-bottom-listing" class="wrap-menu-bottom-listing">
                        <ul></ul>
                    </div>
                    <div class="wrap-menu-bottom-listing-right">&nbsp;</div>
                </div>
            </nav>

            @include('news.partials.footer')
            <div id="divAdLeft"></div>
            <div id="divAdRight"></div>
            <!--Designed by Web123.vn™-->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".menu_toggle").click(function () {
        $(".main-menu-listing").toggle();
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="{{asset('news/js/multislider.js')}}"></script>
<script>
    $('#basicSlider').multislider({
        continuous: true,
        duration: 2000
    });
    $('#mixedSlider').multislider({
        duration: 750,
        interval: 3000
    });
</script>
</body>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
</body>

</html>