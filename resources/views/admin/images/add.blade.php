@extends('admin.layout.layout')
@section('title','Hình ảnh')
@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Chuyên Mục
                        <small>Thêm</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{ $err }}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('flash_success'))
                        <div class="alert alert-success">
                            {{ session('flash_success') }}
                        </div>
                    @endif

                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab" aria-expanded="true">Đăng bài về ảnh</a>
                                </li>
                                <li class=""><a href="#posts" data-toggle="tab" aria-expanded="false">Đăng bải video</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="home">

                                    <form action="{{route('post_images')}}" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="">Tên hình ảnh</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Hình ảnh</label>
                                            <input type="file" class="form-control" name="photo">
                                        </div>
                                        <button type="reset" class="btn btn-default btn-sm">Làm mới</button>
                                        <button type="submit" class="btn btn-success btn-sm">Đăng bài</button>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="posts">
                                    <form action="{{route('post_video')}}" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="">Tựa đề video</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Link video</label>
                                            <input type="text" class="form-control" name="link">
                                        </div>
                                        <button type="reset" class="btn btn-default btn-sm">Làm mới</button>
                                        <button type="submit" class="btn btn-success btn-sm">Đăng bài</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@stop