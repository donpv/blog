@extends('admin.layout.layout')
@section('title','Cài đặt trang tin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cài đặt trang tin tức

                    </h1>
                </div>
                <div class="col-md-6">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{ $err }}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('errfile'))
                        <div class="alert alert-danger">
                            <strong>{{session('errfile')}}</strong>
                        </div>
                    @endif
                    @if(isset($setting))
                        <form action="{{route('post_update')}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="form-group">
                                <label>Tên công ty</label>
                                <input type="text" name="name" id="title" class="form-control" value="{{ $setting->name}}" >
                            </div>

                            <div class="form-group">
                                <label for="">Slogan</label>
                                <input type="text" name="slogan" class="form-control" value="{{$setting->slogan}}">
                            </div>

                            <div class="form-group">
                                <label for="">Logo</label>
                                <input type="file" class="form-control" name="logo">
                            </div>

                            <div class="form-group">
                                <label for="">Banner</label>
                                <input type="file" class="form-control" name="banner">
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" name="email" value="{{$setting->email}}">
                            </div>

                            <div class="form-group">
                                <label for="">Số điện thoại</label>
                                <input type="text" class="form-control" value="{{$setting->phone}}" name="phone">
                            </div>

                            <div class="form-group">
                                <label for="">Địa chỉ công ty</label>
                                <textarea name="address" id="" class="form-control">{{$setting->address}}</textarea>
                            </div>

                            <button type="reset" class="btn btn-default">Làm Mới</button>
                            <button type="submit" class="btn btn-primary">Thêm</button>

                        </form>
                    @else
                        <form action="{{route('post-setting')}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="form-group">
                                <label>Tên công ty</label>
                                <input type="text" name="name" id="title" class="form-control" value="{{ old('name')}}" placeholder="Nhập tên">
                            </div>

                            <div class="form-group">
                                <label for="">Slogan</label>
                                <input type="text" name="slogan" class="form-control" value="{{old('slogan')}}" placeholder="Slogan">
                            </div>

                            <div class="form-group">
                                <label for="">Logo</label>
                                <input type="file" class="form-control" name="logo">
                            </div>

                            <div class="form-group">
                                <label for="">Banner</label>
                                <input type="file" class="form-control" name="banner">
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Email">
                            </div>

                            <div class="form-group">
                                <label for="">Số điện thoại</label>
                                <input type="text" class="form-control" value="{{old('phone')}}" name="phone" placeholder="Số điện thoại">
                            </div>

                            <div class="form-group">
                                <label for="">Địa chỉ công ty</label>
                                <textarea name="address" id="" class="form-control">{{old('address')}}</textarea>
                            </div>

                            <button type="reset" class="btn btn-default">Làm Mới</button>
                            <button type="submit" class="btn btn-primary">Thêm</button>

                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop