@extends('admin.layout.layout')
@section('title','Thêm slider')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">SLIDER
                        <small>Thêm</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{ $err }}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('flash_success'))
                        <div class="alert alert-success">
                            {{ session('flash_success') }}
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <form action="" method="POST" enctype="multipart/form-data"Ω>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Tên slider</label>
                            <input class="form-control" name="name" id="name" placeholder="Tên SLIDER"/>
                        </div>

                        <div class="form-group">
                            <label>Slider ảnh</label>
                            <input type="file" class="form-control" name="photo" id="photo" />
                        </div>

                        <div class="form-group">
                            <label>Link dẫn</label>
                            <input type="text" class="form-control" name="link" id="link" />
                        </div>

                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea name="discription" id="discription" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Trạng thái </label>
                            <select name="status" id="" class="form-control">
                                <option value="0">Kích hoạt</option>
                                <option value="1">Không kích hoạt</option>
                            </select>
                        </div>

                        <button type="reset" class="btn btn-default">Làm Mới</button>
                        <button type="submit" class="btn btn-primary">Thêm</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
@stop