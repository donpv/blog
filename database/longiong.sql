-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th6 17, 2018 lúc 12:01 PM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `longiong`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'author',
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `role`, `avatar`, `birthday`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'fi4Sib7fAQ@gmail.com', '$2y$10$01pWvVu7SR8Ie6jZtvycYOSfIZs/8DC4RanQ3NPZzY.BmcnOYcr2a', 'admin', 'upload/profile/atai_12112387_432816510262523_59285296670757517_n.jpg', '2017-10-22', 'J6Nq9sBSQ7cd42jrfNADySGiasHaSBRrP8gEzTnuA6wbQLfSXw04zwLvIlID', NULL, '2017-10-05 03:28:21'),
(3, 'test', 'abcd@gmail.com', '$2y$10$ODkE37AI6ryN6krwBTVC7.DW9GyY8bG11cFH0YAZPNjPS5h67j8HO', 'author', NULL, NULL, NULL, '2017-10-10 11:15:42', '2017-10-10 11:15:42'),
(4, 'sdasdasd', 'adminasdasd', '$2y$10$wwOR6Qg3qVQncY41gRsCBOBpoghPwnl9X./pqh91cPD2LCuB3psAK', 'author', NULL, NULL, NULL, '2017-10-15 18:38:27', '2017-10-15 18:38:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `show`, `slug`, `parent_id`, `created_at`, `updated_at`) VALUES
(15, 'Giới thiệu', 0, 'gioi-thieu', 0, '2018-06-16 17:02:34', '2018-06-16 17:02:34'),
(16, 'Lịch sử', 0, 'lich-su', 15, '2018-06-16 17:02:41', '2018-06-16 17:02:41'),
(17, 'Cơ cấu tổ chức', 0, 'co-cau-to-chuc', 15, '2018-06-16 17:02:47', '2018-06-16 17:02:47'),
(18, 'Nghiên cứu khoa học', 1, 'nghien-cuu-khoa-hoc', 0, '2018-06-16 17:11:55', '2018-06-16 17:11:55'),
(19, 'Con giống', 1, 'con-giong', 0, '2018-06-17 07:49:58', '2018-06-17 07:49:58'),
(20, 'Tin tức', 1, 'tin-tuc', 0, '2018-06-17 07:50:08', '2018-06-17 07:50:08'),
(21, 'Liên hệ', 0, 'lien-he', 0, '2018-06-17 07:50:22', '2018-06-17 07:50:22'),
(22, 'Tìm kiếm tra cứu', 0, 'tim-kiem-tra-cuu', 0, '2018-06-17 07:50:43', '2018-06-17 07:50:43'),
(23, 'Video', 0, 'video', 0, '2018-06-17 07:51:01', '2018-06-17 07:51:01'),
(24, 'Ảnh', 0, 'anh', 0, '2018-06-17 07:51:28', '2018-06-17 07:51:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_09_20_151220_create_categories_table', 1),
(3, '2017_09_28_143149_create_admin_table', 1),
(4, '2017_09_20_151116_create_posts_table', 2),
(5, '2017_09_20_153825_create_files_table', 3),
(6, '2017_09_20_151239_create_tags_table', 4),
(7, '2017_10_01_155559_creat_table_post_tag', 5),
(8, '2014_10_12_000000_create_users_table', 6),
(9, '2017_10_09_200454_add_feture_to_posts', 7),
(10, '2017_10_09_200814_add_feture_to_posts', 8),
(11, '2017_10_09_201053_add_feture_to_posts', 9),
(12, '2017_10_09_201200_add_feture_to_posts', 10),
(13, '2017_10_09_201356_add_feture_to_posts', 11),
(14, '2017_10_09_202013_add_feture_to_posts', 12),
(15, '2017_10_09_202500_add_thumb_to_files', 13),
(16, '2018_05_09_133853_create_settings_table', 14),
(17, '2018_05_16_144449_create_sliders_table', 14);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `post_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `hot` smallint(6) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL,
  `feture` mediumtext COLLATE utf8_unicode_ci,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `description`, `slug`, `view`, `post_type`, `hot`, `status`, `user_id`, `feture`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Mê tít chuồng gà thông minh mà rẻ của anh nông dân khởi nghiệp lần 2', '<p>asdasdasdasdasdasd</p>', 'asdasdasd', 'me-tit-chuong-ga-thong-minh-ma-re-cua-anh-nong-dan-khoi-nghiep-lan-2', 7, 'text', 1, 1, 1, 'upload/posts/r6WM_Card-STS-HanQuoc-7-6.jpg', 18, '2018-06-17 07:18:21', '2018-06-17 09:58:42'),
(2, 'Hon chien nhieu nguoi chet', '<p style=\"text-align:justify\">Phát biểu góp ý cho dự án Luật Chăn nuôi chiều qua, Thủ tướng Chính phủ Nguyễn Xuân Phúc nhấn mạnh mục đích quan trọng của Luật Chăn nuôi là muốn tiến lên sản xuất lớn. Vừa qua ngành chăn nuôi có sự cố gắng lớn, đã xuất hiện nhiều mô hình sản xuất hàng hoá lớn, đặc biệt là chăn nuôi, tuy nhiên sản xuất còn bấp bênh do chúng ta còn thiếu hệ thống luật pháp quản lý.<br />\r\n<br />\r\n<img alt=\"\" src=\"http://gagiongdabaco.com.vn/images/2018/06/09/0-d1.jpeg\" style=\"border:0px; width:493px\" /><br />\r\n&nbsp;<em>Thủ tướng Nguyễn Xuân Phúc phát biểu thảo luận tổ chiều 7.6</em><br />\r\n<br />\r\n&nbsp;<br />\r\nTrong quá trình xây dựng luật, Bộ NN&amp;PTNT đã làm hết sức tận tình, Bộ trưởng Nguyễn Xuân Cường đã rất lăn lộn, tận tuỵ với công việc, với bà con và đã trao đi đổi lại tranh luận rất nhiều tại Quốc hội về vấn đề này.<br />\r\n&nbsp;<br />\r\n\"Về cơ bản tôi nhất trí với các nội dung chính của dự án luật, nhưng tại điều 5 về việc nhà nước có chính sách gì để tiến tới sản xuất lớn trong chăn nuôi, trong này có điều tôi rất quan tâm chính là quy hoạch chăn nuôi phải làm sao để phát huy thế mạnh so sánh từng khu vực. Sản xuất hiện nay vẫn chủ yếu manh mún, nhỏ lẻ thành thói quen\" - Thủ tướng nói.<br />\r\n&nbsp;<br />\r\nVề vấn đề đào tạo nguồn nhân lực ngành chăn nuôi, tới đây phải làm tốt hơn, hiện nay một số trường về nông nghiệp như Học viện Nông nghiệp vẫn ít đào tạo chăn nuôi thú y, chưa quý&nbsp; trọng ngành này trong khi ngành đang tạo ra của cải vật chất lớn cho đất nước. Việt Nam là đất nước nhiệt đới, có nhiều lợi thế so với các nước ôn đới, có nhân lực tốt, có đầu tư, nếu có nguồn lao động có trình độ, có khoa học công nghệ tốt thì nhất định chúng ta sẽ thành công trong chăn nuôi.<br />\r\n&nbsp;<br />\r\nBên cạnh đó, Thủ tướng nhấn mạnh phải lưu ý công tác xúc tiến thương mại, đầu ra: Hiện nay có hiện tượng dư thừa trong nhiều lĩnh vực nông sản. Lúa gạo vụ đông xuân năm nay thu hoạch tăng thêm 1,2 triệu tấn, cây lúa tương đối rõ vì có thị trường, nhưng chăn nuôi thì lúc lên lúc xuống, nguyên nhân chính do thị trường của chúng ta không ổn định. Đơn cử như chăn nuôi lợn, năm ngoái giá rất thấp, nhưng năm nay giá tăng rất nhanh. Điều này liên quan nhiều đến công tác thị trường, đầu ra, phải làm sao phát triển tốt chăn nuôi để phục vụ thị trường trong nước, tiến tới xuất khẩu.<br />\r\n&nbsp;<br />\r\n\"Hiện nay xuất khẩu sản phẩm chăn nuôi vẫn thấp quá, chủ yếu vẫn là rau quả. Phải có giải pháp đẩy mạnh xuất khẩu hơn nữa, đặc biệt về lâu dài, vì tương lai ngành nông nghiệp bền vững chúng ta phải hạn chế xuất khẩu tiểu ngạch, tăng chính ngạch. Muốn làm được điều này thì Nhà nước phải ra tay, phải có cuộc cách mạng trong chăn nuôi, nếu không đổi mới sẽ rất khó khăn trong phát triển ngành này\" - Thủ tướng nói.&nbsp;<br />\r\n<br />\r\n<img alt=\"\" src=\"http://gagiongdabaco.com.vn/images/2018/06/09/d2.jpg\" style=\"border:0px; width:493px\" /><br />\r\n<em>Thủ tướng Chính phủ Nguyễn Xuân Phúc phát biểu góp ý về dự án Luật Chăn nuôi</em><br />\r\n<br />\r\nVề quy định an toàn thực phẩm, Thủ tướng cũng cho rằng phải quy định rõ hơn nữa. Hiện nay ở nhiều địa phương vẫn phổ biến kiểu vừa ở vừa nuôi, nhà ở trên vật nuôi ở dưới nên ô nhiễm rất nghiêm trọng.<br />\r\n&nbsp;<br />\r\nLiên quan đến các quy định về nguồn gen quý đối với giống vật nuôi, Thủ tướng cho rằng hiện nay mỗi vùng trên đất nước ta đều có nhiều loại vật nuôi đặc sản cần phải bảo vệ, lưu giữ, ví dụ như lợn Móng Cái, gà đen, gà đặc sản, cá cảnh… Cần phát huy gen quý của đất nước ta bằng những chính sách hợp lý. Vì thực tế hiện nay, người dân Việt Nam vẫn không thích ăn thịt gà công nghiệp, trứng công nghiệp, điều đó cho thấy lợi thế vật nuôi đặc sản vẫn đóng góp một phần rất lớn trong cơ cấu nông nghiệp, tăng thu nhập cho nông dân.<br />\r\n&nbsp;<br />\r\nĐặc biệt, người đứng đầu Chính phủ cũng lưu ý thêm về vấn đề đăng ký số lượng vật nuôi - điều đang gây tranh cãi nhiều trong dư luận. Thủ tướng nêu câu hỏi: \"Việc đăng ký này đối với bà con liệu có khó quá không?. Đối với một nước chăn nuôi phát triển, bắt buộc phải có đăng ký số lượng vật nuôi, nhưng để thực hiện tốt điều này, chúng ta cần cải cách thủ tục hành chính, tránh rườm rà, tạo thuận lợi hơn cho người dân. Chính quyền thôn xã phải tăng cường quản lý nắm sát hơn nữa, chứ như hiện nay nhiều nơi xảy ra dịch bệnh mà chính quyền thôn xã không nắm được\".<br />\r\n&nbsp;<br />\r\n\"Tuy việc đăng ký đối với người dân có vẻ phức tạp, hình thức, nuôi con gà con lợn thì có gì phải đăng ký, nhưng muốn quản lý tốt nền sản xuất lớn, muốn bảo vệ môi trường, an toàn thực phẩm, có dự báo tốt về sản lượng, nhu cầu thì chúng ta phải có đăng ký\" - Thủ tướng khẳng định.<br />\r\n&nbsp;</p>\r\n\r\n<div style=\"color: rgb(0, 0, 0); font-family: arial, helvetica, sans-serif; font-size: 14px; text-align: right;\">Theo báo Dân Việt</div>', '\'Tuy việc đăng ký số lượng vật nuôi đối với người dân có vẻ phức tạp, hình thức, nhưng muốn quản lý tốt nền sản xuất lớn, muốn bảo vệ môi trường, an toàn thực phẩm, có dự báo tốt thì phải có đăng ký\' - Thủ tướng nói tại phiên họp tổ chiều 7/6 khi góp ý về dự án Luật Chăn nuôi.', 'hon-chien-nhieu-nguoi-chet', 23, 'text', 1, 1, 1, 'upload/posts/5V1h_Screen Shot 2018-06-15 at 23.04.46.png', 18, '2018-06-17 07:18:58', '2018-06-17 08:52:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `photo`, `link`, `discription`, `status`, `created_at`, `updated_at`) VALUES
(2, 'chuanweb', 'upload/sliders/lyEi_Screen Shot 2018-06-15 at 23.04.46.png', 'https://www.youtube.com/watch?v=79hxX0rOYII', 'asdasd', 0, '2018-06-17 07:34:09', '2018-06-17 07:34:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `admin_name_unique` (`name`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `admin` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
