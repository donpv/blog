<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Route for user */
Route::get('/', 'PagesController@getindex')->name('index');
/* Display front */
Route::get('category/{slug}', 'PagesController@getCategory')->name('category_slug');
Route::get('post/{slug}.html', 'PagesController@getPost')->name('post_slug');
Route::get('tag/{tag}', 'PagesController@getTag');
Route::get('author/{name}', 'PagesController@getAuthor');
Route::get('search', 'PagesController@getSearch')->name('search');
Route::get('anh','PagesController@getImages')->name('images');
Route::get('video','PagesController@getVideo')->name('video');
Route::get('contact.html', 'PagesController@getContact')->name('contact');


Route::get('login', 'LoginController@getLogin');
Route::post('login', 'LoginController@postLogin')->name('login');
Route::get('logout', 'LoginController@getLogout')->name('logout');

/*Group router for author and admin */
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@getdashbroad')->name('dashbroad');
    /* Group for profile */
    Route::get('profile', 'Profilecontroller@getProfile')->name('profile');
    Route::post('profile/update', 'Profilecontroller@profileUpdate');
    Route::post('profile/updatePassword','Profilecontroller@updatePassword')->name('update_password');

    /* Group post*/
    Route::prefix('post')->group(function () {
        Route::get('/', 'PostController@getList')->name('list-post');
        Route::get('add', 'PostController@getAdd')->name('add_post');
        Route::put('updateStatus', 'PostController@updateStatus');
        Route::put('updateHot', 'PostController@updateHot');
        Route::post('add', 'PostController@postAdd')->name('post_add_post');
        Route::get('update/{id}', 'PostController@getUpdate')->name('update_post');
        Route::post('update/{id}', 'PostController@postUpdate');
        Route::get('delete/{id}', 'PostController@getDelete');
    });

    /* Group for admin */
    Route::middleware(['role'])->group(function () {
        /* Group category */
        Route::prefix('category')->group(function () {
            Route::get('/', 'CategoryController@getList')->name('list_category');
            Route::get('add', 'CategoryController@getAdd')->name('add_category');
            Route::post('add', 'CategoryController@postAdd')->name('post_add_category');
            Route::get('data', 'CategoryController@dataTable')->name('data');
            Route::post('update', 'CategoryController@postUpdate');
            Route::delete('delete', 'CategoryController@delete');
        });
        /* Group file */
        Route::prefix('tag')->group(function () {
            Route::get('/', 'TagController@getList')->name('list-tag');
            Route::get('data', 'TagController@dataTable')->name('data-tag');
            Route::post('add', 'TagController@postAdd');
            Route::put('update', 'TagController@putUpdate');
            Route::delete('delete', 'TagController@delete');
        });
        /* Group author */
        Route::prefix('author')->group(function () {
            Route::get('/', 'AdminController@getList')->name('list-author');
            Route::get('data', 'AdminController@dataTable')->name('data-author');
            Route::post('add', 'AdminController@postAdd');
            Route::delete('delete', 'AdminController@delete');
        });

        Route::prefix('slider')->group(function () {
            Route::get('/', 'SliderController@getlist')->name('list-slider');
            Route::get('add', 'SliderController@getAdd')->name('get-add-slider');
            Route::post('add', 'SliderController@postAdd')->name('post-add-slider');
            Route::get('update/{id}', 'SliderController@getUpdate')->name('get-update');
            Route::post('update/{id}','SliderController@postUpdate')->name('post-update');
            Route::put('updateStatus', 'SliderController@updateStatus')->name('updateStatus');
            Route::get('delete/{id}','SliderController@delete');

        });

        Route::prefix('anh')->group(function () {
           Route::get('/','ImagesController@getlist')->name('list_images');
           Route::get('add','ImagesController@getAdd')->name('get_add_images');
           Route::post('images','ImagesController@postImages')->name('post_images');
           Route::post('video','ImagesController@postVideo')->name('post_video');
           Route::get('delete/{id}','ImagesController@delete')->name('delete');
        });

        Route::get('setting', 'SettingController@index')->name('setting');
        Route::post('setting', 'SettingController@postSetting')->name('post-setting');
        Route::post('setting/update', 'SettingController@postUpdate')->name('post_update');

    });
});
